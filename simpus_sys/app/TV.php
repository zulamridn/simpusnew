<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TV extends Model
{
  protected $table = 'table_setting_tv';
  protected $fillable = [ 'vidio', 'text', 'volume_none', 'volume_oncall'];
}
