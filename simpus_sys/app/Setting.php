<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  protected $table = 'table_settings';
  protected $fillable = [ 'logo', 'nama' , 'alamat','back1', 'back2', 'no_ambulance' , 'jam_awal', 'jam_akhir','batas_online', 'vidio', 'text', 'volume_none', 'volume_oncall'];
}
