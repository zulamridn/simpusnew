<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelayanan extends Model
{
  protected $table = 'table_pelayanan_master';
  protected $fillable = [ 'nama_pelayanan', 'id_dokter', 'kode_antrian', 'username', 'password'];
}
