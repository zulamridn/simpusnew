<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Printer extends Model
{
  protected $table = 'table_printer';
  protected $fillable = [ 'loket', 'app_key', 'app_port'];
}
