<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Onlinecount extends Model
{
  protected $table = 'table_online_count';
  protected $fillable = ['count'];
}
