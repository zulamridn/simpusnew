<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
  protected $table = 'table_tarif_master';
  protected $fillable = [ 'nama_tindakan', 'tarif'];
}
