<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
  protected $table = 'table_pasien';
  protected $fillable = [ 'nik', 'no_rm', 'nama', 'jenis_kelamin','alamat','tempat_lahir', 'tanggal_lahir', 'no_hp', 'nama_kepala_keluarga', 'status', 'kunjungan', 'old_rm', 'agama', 'pekerjaan'];
}
