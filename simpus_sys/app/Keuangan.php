<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keuangan extends Model
{
    protected $table = 'table_keuangan';
    protected $fillable = ['no_registrasi', 'total'];
}
