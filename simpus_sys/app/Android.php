<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Android extends Model
{
  protected $table = 'table_user_android';
  protected $fillable = [ 'nik','nama', 'email', 'no_hp', 'alamat'];
}
