<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
  protected $table = 'table_nilai';
  protected $fillable = [ 'id_pelayanan','nilai'];
}
