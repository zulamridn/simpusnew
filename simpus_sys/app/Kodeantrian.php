<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kodeantrian extends Model
{
  protected $table = 'table_kode_antrian';
  protected $fillable = [ 'kode', 'use'];
}
