<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekapoli extends Model
{
  protected $table = 'table_rekap_poli';
  protected $fillable = ['kode_antrian_poli', 'total', 'dipanggil','sisa'];
}
