<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
  protected $table = 'table_download';
  protected $fillable = [ 'nama_file', 'link_file', 'keterangan'];
}
