<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
  protected $table = 'table_dokter_master';
  protected $fillable = [ 'id_dokter', 'nama' , 'spesialis'];
}
