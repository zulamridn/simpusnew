<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnosa extends Model
{
  protected $table = 'table_diagnosa';
  protected $fillable = [ 'no_registrasi', 'no_rm', 'nik', 'nama_poli', 'nama_dokter','keluhan', 'diagnosa', 'catatan', 'tindakan'];
}
