<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tindakan extends Model
{
  protected $table= 'table_tindakan';
  protected $fillable  = ['no_registrasi', 'id_tindakan'];
}
