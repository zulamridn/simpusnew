<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekamobat extends Model
{
  protected $table = 'table_rekamobat';
  protected $fillable = ['no_register', 'no_rm', 'nama_obat'];
}
