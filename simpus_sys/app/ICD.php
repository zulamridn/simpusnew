<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ICD extends Model
{
  protected $table = 'table_icd';
  protected $fillable = [ 'icd', 'nama_penyakit'];
}
