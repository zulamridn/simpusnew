<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekap extends Model
{
  protected $table = 'table_rekap_antrian';
  protected $fillable = ['total_antrian', 'dipanggil', 'sisa'];
}
