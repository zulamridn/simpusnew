<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Onlinecount;
use App\Setting;
use App\Rekap;
use App\Pasien;
use App\Pelayanan;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class RealTimeController extends Controller
{

  public function get_name_poli_antrian(){
    //$data = Pelayanan::all();
    $data = DB::table('table_pelayanan_master')
            ->join('table_dokter_master', 'table_dokter_master.id_dokter', '=', 'table_pelayanan_master.id_dokter')
            ->join('table_rekap_poli', 'table_rekap_poli.kode_antrian_poli', '=', 'table_pelayanan_master.kode_antrian' )
            ->where('table_rekap_poli.created_at', '>=', Carbon::today())
            ->select('table_pelayanan_master.*', 'table_dokter_master.nama', 'table_rekap_poli.dipanggil')
            ->get();
    return response()->json($data);
  }


}
