<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index(){
      $data = Setting::all();
      return view('index',compact('data'));
    }

    public function register(){
      $data = Setting::all();
      return view('register', compact('data'));
    }

    public function login(Request $req){

      $username = $req->username;
      $password = $req->password;

      $cek = User::where('username', $username)->first();

      if($cek != '' && $cek->aktif != '0'){
        if(Hash::check($password, $cek->password)){
          Session::put('id', $cek->id);
          Session::put('name', $cek->name);
          Session::put('avatar', $cek->avatar);
          Session::put('email', $cek->email);
          Session::put('token', $cek->api_token);
          Session::put('nik', $cek->nik);
          Session::put('login', 'User');


          if($cek->tempatlahir == ''){
            return redirect('/useredit');
          }else{
            return redirect('/');
          }
        }else{
          return redirect()->back()->with('message', 'password');
        }
      }else{
        return redirect()->back()->with('message', 'email');
      }


    }
}
