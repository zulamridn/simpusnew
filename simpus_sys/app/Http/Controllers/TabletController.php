<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelayanan;
use App\Dokter;
use App\Nilai;
use Illuminate\Support\Facades\DB;


class TabletController extends Controller
{
    public function getData(){
      //$data = Pelayanan::all();
      $data = DB::table('table_pelayanan_master')
              ->join('table_dokter_master', 'table_dokter_master.id_dokter', '=', 'table_pelayanan_master.id_dokter')
              ->select('table_pelayanan_master.*', 'table_dokter_master.nama')
              ->get();

      return response()->json($data);

    }

    public function getDetail(Request $req){
      $data = DB::table('table_pelayanan_master')
              ->join('table_dokter_master', 'table_dokter_master.id_dokter', '=', 'table_pelayanan_master.id_dokter')
              ->select('table_pelayanan_master.*', 'table_dokter_master.nama')
              ->where('table_pelayanan_master.id', '=', $req->id)
              ->get();

      return response()->json($data);
    }

    public function giveNilai(Request $req){
      $data = Nilai::create([
        'id_pelayanan' => $req->id,
        'nilai' => $req->nilai
      ]);

      return response()->json(['sukses' => '1']);
    }


}
