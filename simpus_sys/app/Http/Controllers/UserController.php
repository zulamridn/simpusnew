<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function create_user(Request $req){

      $create = Users::create([
        'username' => $req->username,
        'password' => Hash::make($req->password),
        'akses' => $req->akses
      ]);

      return response()->json(['status' => '1']);

    }
}
