<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarif;

class TarifController extends Controller
{
  public function view(){

    $data = Tarif::all();
    return view('root/tarif_view', compact('data'));

  }

  public function create(Request $req){

    $data = Tarif::create([
      'nama_tindakan' => $req->nama_tindakan,
      'tarif' => $req->tarif,
    ]);

    return redirect('/tarif')->with('pesan', 'sukses');

  }

  public function update_link(Request $req){

    $data = Tarif::where('id', $req->id)->get();
    return view('root.tarif_edit', compact('data'));

  }

  public function update(Request $req){
    $data = Tarif::where('id', $req->id)->update([
      'nama_tindakan' => $req->nama_tindakan,
      'tarif' =>$req->tarif
    ]);

    return redirect('/tarif')->with('pesan', 'edit');
  }

  public function delete(Request $req){

    $data = Tarif::where('id', $req->id)->delete();
    return redirect()->back()->with('pesan', 'sukses');

  }
}
