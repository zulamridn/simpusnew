<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Onlinecount;
use App\Setting;
use App\Rekap;
use App\Pasien;
use App\Rekapoli;
use App\Diagnosa;
use App\Tarif;
use App\Biaya;
use App\Tindakan;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;


class PasienController extends Controller{

  public function create_pasien(Request $req){

    //membuat kode rm
    //RUMUS Urutan Ajad Pertama Nama Kepala Keluarga + Status dalam Keluarga + autoicrement
    $dataterakhir = Pasien::all()->last();
    $nomorterakhir = substr($dataterakhir['no_rm'], 4);
    $insertnomorterakhir = $nomorterakhir +1;
    if($insertnomorterakhir <10){
      $nomorregis = '000'.$insertnomorterakhir;
    }elseif ($insertnomorterakhir<100 && $insertnomorterakhir>=10) {
      $nomorregis = '00'.$insertnomorterakhir;
    }elseif ($insertnomorterakhir<1000 && $insertnomorterakhir>=100) {
      $nomorregis = '0'.$insertnomorterakhir;
    }else{
      $nomorregis = $insertnomorterakhir;
    }
    $karakterpertamakk = substr($req->nama_kepala_keluarga, 0, 1);
    $kktonumber = ord(strtolower($karakterpertamakk)) - 96;

    if($kktonumber < 10){
      $nomorkk = '0'.$kktonumber;
    }else{
      $nomorkk = $kktonumber;
    }

    $statuskeluarga = $req->status_keluarga;

    if($statuskeluarga == '01'){
      $statusstring = "Kepala Keluarga";
    }elseif ($statuskeluarga == '02') {
      $statusstring = "Istri";
    }elseif ($statuskeluarga == '03') {
      $statusstring = "Anak";
    }else{
      $statusstring = "Anggota Keluarga Lainnya";
    }


    $data = Pasien::create([
      'nik' => $req->nik,
      'old_rm' => $oldrm,
      'no_rm' => $nomorkk.$statuskeluarga.$nomorregis,
      'nama' => $req->nama,
      'jenis_kelamin' => $req->jenis_kelamin,
      'alamat' => $req->alamat,
      'tempat_lahir' => $req->tempat_lahir,
      'tanggal_lahir' => $req->tanggal_lahir,
      'no_hp' => $req->no_hp,
      'status' => $statusstring,
      'nama_kepala_keluarga' => $req->nama_kepala_keluarga,
      'agama' => $req->agama,
      'pekerjaan' => $req->pekerjaan,
      'kunjungan' => '0',
    ]);

    return response()->json(['status' => '1']);

  }

  public function get_pasien_data(Request $req){

    $today = Carbon::now();
    $data = Pasien::where('nik', '=', $req->nik)->get()->last();

    if($data != null){
      $tangal_lahir = Carbon::parse($data['tanggal_lahir']);
      $bulan = date('m', strtotime($data['tanggal_lahir']));
      $tanggal = date('d', strtotime($data['tanggal_lahir']));
      $tahun = date('Y', strtotime($data['tanggal_lahir']));
      $tanggallahir = $tanggal." ".$this->bulan($bulan)." ".$tahun;
      $nomor_rm = $data['no_rm'];
      $nik = $data['nik'];
      $nama = $data['nama'];
      $umur= $today->diff($tangal_lahir);
      if($data['jenis_kelamin'] == "L" ){
        $jk = "Laki Laki";
      }else{
        $jk = "Perempuan";
      }

      return response()->json([
        'status' => '1',
        'no_rm' => $nomor_rm,
        'nik' => $nik,
        'nama' => $nama,
        'tempat_lahir' => $data['tempat_lahir'],
        'tanggal_lahir' => $tanggallahir,
        'umur' => $umur->y,
        'jk' => $jk,
        'alamat' => $data['alamat']
      ]);
    }else{
      return response()->json([
        'status' => '0'
      ]);
    }

  }

  public function get_pasien_datarm(Request $req){

    $today = Carbon::now();
    $data = Pasien::where('no_rm', '=', $req->rm)->get()->last();

    if($data != null){
      $tangal_lahir = Carbon::parse($data['tanggal_lahir']);
      $bulan = date('m', strtotime($data['tanggal_lahir']));
      $tanggal = date('d', strtotime($data['tanggal_lahir']));
      $tahun = date('Y', strtotime($data['tanggal_lahir']));
      $tanggallahir = $tanggal." ".$this->bulan($bulan)." ".$tahun;
      $nomor_rm = $data['no_rm'];
      $nik = $data['nik'];
      $nama = $data['nama'];
      $umur= $today->diff($tangal_lahir);
      if($data['jenis_kelamin'] == "L" ){
        $jk = "Laki Laki";
      }else{
        $jk = "Perempuan";
      }

      return response()->json([
        'status' => '1',
        'no_rm' => $nomor_rm,
        'nik' => $nik,
        'nama' => $nama,
        'tempat_lahir' => $data['tempat_lahir'],
        'tanggal_lahir' => $tanggallahir,
        'umur' => $umur->y,
        'jk' => $jk,
        'alamat' => $data['alamat']
      ]);
    }else{
      return response()->json([
        'status' => '0'
      ]);
    }

  }

  public function edit_pasien(Request $req){

    $rm = $req->rm;

    $data = Pasien::where('no_rm', $rm)->get();

    return view('root.pasien_edit', compact('data'));

  }

  public function update_pasien(Request $req){

    $data = Pasien::where('no_rm', $req->no_rm)->update([
      'nik' => $req->nik,
      'nama' => $req->nama,
      'jenis_kelamin' => $req->jenis_kelamin,
      'alamat' => $req->alamat,
      'tempat_lahir' => $req->tempat_lahir,
      'tanggal_lahir' => $req->tanggal_lahir,
      'no_hp' => $req->no_hp,
    ]);

    return redirect('/pasien_list');

  }

  public function pendaftaran_poli(Request $req){


    $today = Carbon::now();
    $sekarang = date('Y-m-d');
    $id_antrian = $req->id_antrian;
    $nik = $req->nik;
    $antrian_poli = $req->kode_antrian;

    //mencari umur dan jeniskelamin pasien
    $data = Pasien::where('no_rm', '=', $req->id_pasien)->get()->last();
    $tangal_lahir = Carbon::parse($data['tanggal_lahir']);
    $diff_umur= $today->diff($tangal_lahir);
    $umur = $diff_umur->y;
    $jeniskelamin = $data['jenis_kelamin'];


    $cek_status_antrian = Pendaftaran::where('kode_antrian', '=', $req->id_antrian)->get()->last();
    $id_terdaftar = $cek_status_antrian['id_pasien'];
    if($id_terdaftar == null){

      $rekap_poli = Rekapoli::where('kode_antrian_poli', '=',$req->kode_antrian)->get()->last();
      if($rekap_poli == null){
          $create = Rekapoli::create([
            'kode_antrian_poli' => $req->kode_antrian,
            'total' => '1',
            'dipanggil' => '0',
            'sisa' => '1'
          ]);
          $antrian = '1';
      }else{
        $tanggal = $rekap_poli['created_at']->format('Y-m-d');
        if($tanggal == $sekarang){
          $antrian = $rekap_poli['total']+1;
          $update = Rekapoli::where('kode_antrian_poli', '=',$req->kode_antrian)->update([
            'total' =>$antrian,
            'sisa' =>$rekap_poli['sisa']+1
          ]);
        }else{
          $create = Rekapoli::create([
            'kode_antrian_poli' => $req->kode_antrian,
            'total' => '1',
            'dipanggil' => '0',
            'sisa' => '1'
          ]);
          $antrian = '1';
        }
      }

      if($antrian < 10){
        $nomorantrianpoli = $req->kode_antrian.'00'.$antrian;
      }elseif ($antrian >=10 && $antrian<100) {
        $nomorantrianpoli = $req->kode_antrian.'0'.$antrian;
      }else{
        $nomorantrianpoli = $req->kode_antrian.$antrian;
      }


      $update_antrian_master = Pendaftaran::where('kode_antrian', '=', $id_antrian)->update([
        'id_pasien' => $req->id_pasien,
        'nomor_poli' => $nomorantrianpoli,
        'kode_poli' => $req->kode_antrian,
        'antrian_poli' => $antrian,
        'jenis_kelamin' => $jeniskelamin,
        'umur' => $umur
      ]);

      // $connector = new WindowsPrintConnector("smb://DESKTOP-07QF3KV/PRINTPOSEPSON");
      // $printer = new Printer($connector);
      // $printer -> setJustification(Printer::JUSTIFY_CENTER);
      // $printer -> text("Puskesmas Sambas\n");
      // $printer -> text("Dinas Kesehatan Kabupaten Sambas\n");
      // $printer -> text("\n");
      // $printer -> setTextSize(6,6);
      // $printer -> text($nomorantrianpoli."\n");
      // $printer -> text("\n");
      // $printer -> cut();
      // $printer -> setJustification(Printer::JUSTIFY_LEFT);
      // $printer -> setTextSize(2,2);
      // $printer -> text("No :".$nomorantrianpoli."\n");
      // $printer -> text("Nama :".$data['nama']."\n");
      // $printer -> text("RM :".$req->id_pasien."\n");
      // $printer -> text("\n");
      // $printer -> cut();
      // $printer -> close();
      
     
      return response()->json(['status' => '1', 'antrian' => $nomorantrianpoli]);
    }else{
      return response()->json(['status' => '0']);
    }

  }

  public function printAntrianPoli(){
    try {
      $connector = new WindowsPrintConnector("smb://DESKTOP-07QF3KV/PRINTPOSEPSON");
      $printer = new Printer($connector);
      $printer -> text("Hello World");
      $printer -> cut();
      
      $printer -> close();
      
      
  } catch (Exception $e) {
      echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
  }
  }


  public function create_diagnosa(Request $req){
    
    $cekdata = Diagnosa::whereDate('created_at', DB::raw('CURDATE()'))->where('no_registrasi', $req->registrasi)->count();

    if($cekdata === 0 ){
      $create = Diagnosa::create([
        'no_registrasi' => $req->registrasi,
        'no_rm' => $req->rm,
        'nik' => $req->nik,
        'nama_poli' => $req->nama_poli,
        'nama_dokter' => $req->nama_dokter,
        'keluhan' => $req->keluhan,
        'diagnosa' => $req->diagnosa,
        'catatan' => $req->catatan,
      ]);
  
      $tindakan = explode(",", $req->tindakan);
      $tindakan_count = count($tindakan);
  
      for($i = 0; $i<$tindakan_count; $i++){
  
        $create = Tindakan::create([
          'no_registrasi' => $req->registrasi,
          'id_tindakan' => $tindakan[$i]
        ]);
      }
  
      $update = Pendaftaran::where('kode_antrian', $req->registrasi)->update([
        'poli' => '1',
      ]);
  
      return response()->json(['status' => '1']);
    }else{
      return response()->json(['status' => '0']);
    }

    

  }

  public function bulan($bulan){

      switch ($bulan) {
        case 1: return "Januari";
        case 2: return "Februari";
        case 3: return "Maret";
        case 4: return "April";
        case 5: return "Mei";
        case 6: return "Juni";
        case 7: return "Juli";
        case 8: return "Agustus";
        case 9: return "September";
        case 10: return "Oktober";
        case 11: return "November";
        case 12: return "Desember";
      }

    }

}
