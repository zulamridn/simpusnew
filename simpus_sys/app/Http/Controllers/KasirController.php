<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Keuangan;
use App\Tarif;
use App\Tindakan;
use App\Setting;
use App\Printer;
use Illuminate\Support\Facades\DB;

class KasirController extends Controller
{
    public function index(Request $req){
        date_default_timezone_set("Asia/Bangkok");
        //$data = Pendaftaran::where('poli', '1')->whereDate('created_at', DB::raw('CURDATE()'))->get();
        if($req->session()->get('akses') != null && $req->session()->get('akses') == 'kasir' ){
            $data = DB::table('table_pendaftaran')
                    ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
                    ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                    ->where('table_pendaftaran.poli', '=', '1')
                    ->where('table_pendaftaran.kasir', '=', '0')
                    ->select('table_pendaftaran.*', 'table_pasien.nama')
                    ->get();
             $printer = Printer::where('loket', 'kasir')->get();

            return view('root.kasir', compact('data', 'printer'));
        }else{
            return redirect('/loginall');
        }

    }

    public function get_detail(Request $req){
        date_default_timezone_set("Asia/Bangkok");
        $pasien = DB::table('table_pendaftaran')
                ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
                ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                ->where('table_pendaftaran.kode_antrian', '=', $req->registrasi)
                ->select('table_pendaftaran.*', 'table_pasien.nama', 'table_pasien.alamat', 'table_pasien.no_rm', 'table_pasien.no_hp')
                ->get()->last();

        $biaya = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
                ->select('table_tindakan.*', 'table_tarif_master.nama_tindakan', 'table_tarif_master.tarif')
                ->get();

        $biaya_total = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
                ->select('table_tindakan.*', 'table_tarif_master.nama_tindakan', 'table_tarif_master.tarif')
                ->sum('table_tarif_master.tarif');

        $tarif = Tarif::all();
        $setting = Setting::get()->last();

        // $biaya = DB::table('table_tindakan')
        //             ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
        //             ->select('table_tindakan.*')
        //             ->get();

        return view('root.kasir_kwitansi', compact('pasien', 'biaya', 'biaya_total', 'tarif', 'setting'));
        //return response()->json($biaya);

    }

    public function invoice(Request $req){
        date_default_timezone_set("Asia/Bangkok");
        $pasien = DB::table('table_pendaftaran')
                ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
                ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                ->where('table_pendaftaran.kode_antrian', '=', $req->registrasi)
                ->select('table_pendaftaran.*', 'table_pasien.nama', 'table_pasien.alamat', 'table_pasien.no_rm', 'table_pasien.no_hp')
                ->get()->last();

        $biaya = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
                ->select('table_tindakan.*', 'table_tarif_master.nama_tindakan', 'table_tarif_master.tarif')
                ->get();

        $biaya_total = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
                ->select('table_tindakan.*', 'table_tarif_master.nama_tindakan', 'table_tarif_master.tarif')
                ->sum('table_tarif_master.tarif');

        $tarif = Tarif::all();
        $setting = Setting::get()->last();

        return view('root.kasir_invoice', compact('pasien', 'biaya', 'biaya_total', 'tarif', 'setting'));


    }

    public function tambah_biaya(Request $req){
        date_default_timezone_set("Asia/Bangkok");
        $tindakan = explode(",", $req->tindakan);
        $tindakan_count = count($tindakan);

        for($i = 0; $i<$tindakan_count; $i++){

        $create = Tindakan::create([
            'no_registrasi' => $req->registrasi,
            'id_tindakan' => $tindakan[$i]
        ]);
        }

        return response()->json(['status' => '1']);

    }

    public function hapus_biaya(Request $req){

        $hapus = Tindakan::find($req->id)->delete();
        return response()->json(['status' => '1']);

    }

    public function simpan_biaya(Request $req){
        date_default_timezone_set("Asia/Bangkok");
        $insert = Keuangan::create([
            'no_registrasi' => $req->registrasi,
            'total' => $req->total,
        ]);

        $update = Pendaftaran::where('kode_antrian', $req->registrasi)->update([
          'kasir' => '1',
        ]);

        return response()->json(['status' => '1']);

    }
}
