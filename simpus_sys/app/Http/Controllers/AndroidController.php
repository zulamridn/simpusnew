<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Android;
use App\Rekap;
use App\Pendaftaran;
use App\Setting;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AndroidController extends Controller{

  public function createuser(Request $req){

    $create = Android::create([
      'nik' => $req->nik,
      'nama' => $req->nama,
      'email' => $req->email,
      'no_hp' => $req->no_hp,
      'alamat' => $req->alamat
    ]);

    return response()->json(['status' => '1']);

  }

  public function getAntrian(){
    date_default_timezone_set("Asia/Bangkok");
    $data = Rekap::whereDate('created_at', DB::raw('CURDATE()'))->get()->last();

    if($data != null){
      //return response()->json($data);
    }else{
      $insert_rekap = Rekap::create([
        'total_antrian' => '0',
        'dipanggil' => '0',
        'sisa' => '0',
      ]);
    }

    $data2 = Rekap::whereDate('created_at', DB::raw('CURDATE()'))->get();

    return response()->json($data2);

  }

  public function getMy(Request $req){

    $data = Pendaftaran::whereDate('created_at', DB::raw('CURDATE()'))->where('kode_antrian', $req->id)->get()->last();
    if($data != null){
      $nomor = $data->nomor_antrian;
      if($nomor < 10){
        return response()->json(['antrian' => 'A00'.$nomor]);
      }elseif ($nomor<100 && $nomor>9) {
        return response()->json(['antrian' => 'A0'.$nomor]);
      }else{
        return response()->json(['antrian' => 'A'.$nomor]);
      }
    }else{
      $nomor = '0';
      return response()->json(['antrian' => '0']);
    }


  }

  public function getAntianPoli(){

    $data = DB::table('table_rekap_poli')
            ->join('table_pelayanan_master', 'table_pelayanan_master.kode_antrian', '=', 'table_rekap_poli.kode_antrian_poli')
            ->join('table_dokter_master', 'table_dokter_master.id_dokter', '=', 'table_pelayanan_master.id_dokter')
            ->select('table_rekap_poli.*', 'table_pelayanan_master.nama_pelayanan', 'table_pelayanan_master.id_dokter', 'table_dokter_master.nama')
            ->whereDate('table_rekap_poli.created_at', '=', DB::raw('CURDATE()'))
            ->get();

    return response()->json($data);

  }

  public function getNoAmbulace(){
    $data = Setting::select('no_ambulance', 'jam_awal', 'jam_akhir', 'batas_online')->get()->last();

    return response()->json($data);

  }

}
