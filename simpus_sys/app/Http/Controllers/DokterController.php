<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokter;

class DokterController extends Controller
{
    public function view(){

      $data = Dokter::all();
      return view('root/dokter_view', compact('data'));

    }

    public function create(Request $req){

      $data = Dokter::create([
        'id_dokter' => $req->id_dokter,
        'nama' => $req->nama,
        'spesialis' => $req->spesialis
      ]);

      return redirect('/dokter')->with('pesan', 'sukses');

    }

    public function update_link(Request $req){

      $data = Dokter::where('id', $req->id)->get();
      return view('root.dokter_edit', compact('data'));

    }

    public function update(Request $req){
      $data = Dokter::where('id', $req->id)->update([
        'nama' => $req->nama,
        'spesialis' =>$req->spesialis
      ]);

      return redirect('/dokter')->with('pesan', 'edit');
    }

    public function delete(Request $req){

      $data = Dokter::where('id', $req->id)->delete();
      return redirect()->back()->with('pesan', 'sukses');

    }
}
