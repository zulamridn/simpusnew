<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Onlinecount;
use App\Setting;
use App\Rekap;
use App\Pasien;
use App\Pelayanan;
use App\Rekapoli;
use App\Printer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PendaftaranController extends Controller
{
    public function tes_koneksi(){
      return response()->json(['status' => 'Konkesi dengan API server Berhasil']);
    }

    public function index(){
      date_default_timezone_set("Asia/Bangkok");
      $poli = Pelayanan::all();
      $cekdatarekap = Rekap::all()->last();
      $countrekap = Rekap::all()->count();
      $printer = Printer::where('loket', 'pendaftaran')->get();
      $today = date('Y-m-d');
      if($countrekap == 0){
           $defaultrekap = Rekap::create([
          'total_antrian' => '0',
          'dipanggil' => '0',
          'sisa' => '0'
        ]);
      }else{
        $cektanggalterakhir = $cekdatarekap['created_at']->format('Y-m-d');
        if($today != $cektanggalterakhir){
            $defaultrekap = Rekap::create([
              'total_antrian' => '0',
              'dipanggil' => '0',
              'sisa' => '0'
            ]);
        }
      }
      $data = Rekap::all()->last();
      return view('root.pendaftaran', compact('poli', 'data', 'printer'));

    }

    public function indexapi(){
      date_default_timezone_set("Asia/Bangkok");
      $poli = Pelayanan::all();
      $cekdatarekap = Rekap::all()->last();
      $countrekap = Rekap::all()->count();
      $printer = Printer::where('loket', 'pendaftaran')->get();
      $today = date('Y-m-d');
      if($countrekap == 0){
           $defaultrekap = Rekap::create([
          'total_antrian' => '0',
          'dipanggil' => '0',
          'sisa' => '0'
        ]);
      }else{
        $cektanggalterakhir = $cekdatarekap['created_at']->format('Y-m-d');
        if($today != $cektanggalterakhir){
            $defaultrekap = Rekap::create([
              'total_antrian' => '0',
              'dipanggil' => '0',
              'sisa' => '0'
            ]);
        }
      }
      $data = Rekap::all()->last();
      //return view('root.pendaftaran', compact('poli', 'data', 'printer'));
      return response()->json(['poli' => $poli, 'data' => $data]);
    }


    public function get_antrian(Request $req){
      date_default_timezone_set("Asia/Bangkok");

      $today = date('Y-m-d');
      $pendaftaran_count = Pendaftaran::all()->count();
      $last = Pendaftaran::all()->last();
      $online = Onlinecount::all()->last();
      $onlineDate = Onlinecount::whereDate('created_at', DB::raw('CURDATE()'))->count();
      //$today_online= $online['created_at']->format('Y-m-d');
      $online_limit = Setting::all()->last();

      $uniqid = substr(uniqid(time(), true), 16,16);

      if($req->online == '1' ){
        if($onlineDate > 0){
          if($online['count'] < $online_limit['batas_online']){

            $data_cont_insert = Onlinecount::where('id', '=', $online['id'])->update([
              'count' => $online['count']+1,
            ]);
            $create = Pendaftaran::create([
                'nomor_antrian' => $last['nomor_antrian']+1,
                'kode_antrian' => $uniqid,
                'online' => $req->online,
                'bpjs' => $req->bpjs,
                'id_pasien' => $req->id_pasien
            ]);
            $nomor = $last['nomor_antrian']+1;
            $set_total = '1';
            $print = '0';
          }else{
            return response()->json("Maaf Kuota Antrian online Sudah Habis");
          }
        }else{
          $data_cont_insert = Onlinecount::create([
            'count' => '1',
          ]);
           $create = Pendaftaran::create([
                'nomor_antrian' => $last['nomor_antrian']+1,
                'kode_antrian' => $uniqid,
                'online' => $req->online,
                'bpjs' => $req->bpjs,
                'id_pasien' => $req->id_pasien
            ]);
            $nomor = $last['nomor_antrian']+1;
            $set_total = '1';
            $print = '0';
        }
      }else{
        if($pendaftaran_count > 0){
            $today_pendaftaran = $last['created_at']->format('Y-m-d');
            if($today == $today_pendaftaran){
                $data = "Sama";
                $create = Pendaftaran::create([
                    'nomor_antrian' => $last['nomor_antrian']+1,
                    'kode_antrian' => $uniqid,
                    'online' => $req->online,
                    'bpjs' => $req->bpjs,
                    'id_pasien' => $req->id_pasien
                ]);
                $set_total = '1';
                $print = '1';
                $nomor = $last['nomor_antrian']+1;
            }else{
                  $data = "Tidak Sama";
                  $create = Pendaftaran::create([
                    'nomor_antrian' => '1',
                    'kode_antrian' => $uniqid,
                    'online' => $req->online,
                    'bpjs' => $req->bpjs,
                    'id_pasien' => $req->id_pasien
                  ]);
                  $set_total = '1';
                  $print = '1';
                  $nomor = '1';
            }
        }else{
             $data = "Tidak Sama";
                  $create = Pendaftaran::create([
                    'nomor_antrian' => '1',
                    'kode_antrian' => $uniqid,
                    'online' => $req->online,
                    'bpjs' => $req->bpjs,
                    'id_pasien' => $req->id_pasien
                  ]);
                  $set_total = '1';
                  $print = '1';
                  $nomor = '1';
        }
        
      }

      if($set_total == '1'){

        $rekap_last = Rekap::all()->last();
        $rekap_count = Rekap::all()->count();
        
        
        if($rekap_count > 0) {
            $rekap_today = $rekap_last['created_at']->format('Y-m-d');
            if($today == $rekap_today){
              $update_rekap = Rekap::where('id', '=', $rekap_last['id'])->update([
                'total_antrian' => $rekap_last['total_antrian']+1,
                'sisa' => ($rekap_last['total_antrian']+1)-$rekap_last['dipanggil']
              ]);
              $sisa = ($rekap_last['total_antrian']+1)-$rekap_last['dipanggil'];
            }else{
              $insert_rekap = Rekap::create([
                'total_antrian' => '1',
                'dipanggil' => '0',
                'sisa' => '1',
              ]);
              $sisa = '1';
            }
        }else{
            $insert_rekap = Rekap::create([
                'total_antrian' => '1',
                'dipanggil' => '0',
                'sisa' => '1',
              ]);
              $sisa = '1';
        }

      }

      if($print == '1'){
        if($nomor < 10){
          return response()->json(['antrian' => 'A00'.$nomor, 'sisa' => 'Sisa Antrian '.$sisa, 'id' => $uniqid ]);
        }elseif ($nomor<100 && $nomor>9) {
          return response()->json(['antrian' => 'A0'.$nomor, 'sisa' => 'Sisa Antrian '.$sisa, 'id' => $uniqid ]);
        }else{
          return response()->json(['antrian' => 'A'.$nomor, 'sisa' => 'Sisa Antrian '.$sisa, 'id' => $uniqid ]);
        }
      }else{
        if($nomor < 10){
          return response()->json(['antrian' => 'A00'.$nomor, 'sisa' => 'Sisa Antrian '.$sisa, 'id' => $uniqid ]);
        }elseif ($nomor<100 && $nomor>9) {
          return response()->json(['antrian' => 'A0'.$nomor, 'sisa' => 'Sisa Antrian '.$sisa, 'id' => $uniqid ]);
        }else{
          return response()->json(['antrian' => 'A'.$nomor, 'sisa' => 'Sisa Antrian '.$sisa, 'id' => $uniqid ]);
        }
      }

    }

    public function panggil(){
      date_default_timezone_set("Asia/Bangkok");
      $today = date('Y-m-d');
      $cek_data = Rekap::all()->last();
      if($today == $cek_data['created_at']->format('Y-m-d')){
        if($cek_data['sisa'] != '0'){
          $dipanggil = $cek_data['dipanggil']+1;
          $update_panggil = Rekap::where('id', $cek_data['id'])->update([
            'dipanggil' => $dipanggil,
            'sisa' => $cek_data['total_antrian']-$dipanggil
          ]);
          $data = Pendaftaran::where('nomor_antrian', '=', $dipanggil)->get()->last();

          $content      = array(
              "en" => 'A'.$dipanggil
          );
          $hashes_array = array();
          $fields = array(
              'app_id' => "9e3c5524-7a74-4b88-9a0f-846c3b4abf1b",
              'included_segments' => array(
                  'All'
              ),
              'data' => array(
                  "foo" => "bar"
              ),
              'contents' => $content,
              'web_buttons' => $hashes_array
          );

          $fields = json_encode($fields);

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json; charset=utf-8',
              'Authorization: Basic NmExMDdiMDYtN2Y2Yi00N2VmLTkzNzktZGJiZDg3Y2U1ZjA0'
          ));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_HEADER, FALSE);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

          $response = curl_exec($ch);
          curl_close($ch);

          return response()->json(['sisa' => $cek_data['total_antrian']-$dipanggil, 'dipanggil' =>  $dipanggil, 'status' => '1', 'data' => $data]);
        }else{
          return response()->json(['sisa' => '0']);
        }
      }else{
        return response()->json(['status' => '0']);
      }


    }

    public function onesignal(){
        $content      = array(
            "en" => 'English'
        );
        $hashes_array = array();
        $fields = array(
            'app_id' => "9e3c5524-7a74-4b88-9a0f-846c3b4abf1b",
            'included_segments' => array(
                'All'
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NmExMDdiMDYtN2Y2Yi00N2VmLTkzNzktZGJiZDg3Y2U1ZjA0'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        //return $response;
    }

    public function ulang(Request $req){

      $dipanggil = $req->nomorantrian;

      $content      = array(
          "en" => 'A'.$dipanggil
      );
      $hashes_array = array();
      $fields = array(
          'app_id' => "9e3c5524-7a74-4b88-9a0f-846c3b4abf1b",
          'included_segments' => array(
              'All'
          ),
          'data' => array(
              "foo" => "bar"
          ),
          'contents' => $content,
          'web_buttons' => $hashes_array
      );

      $fields = json_encode($fields);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json; charset=utf-8',
          'Authorization: Basic NmExMDdiMDYtN2Y2Yi00N2VmLTkzNzktZGJiZDg3Y2U1ZjA0'
      ));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

      $response = curl_exec($ch);
      curl_close($ch);

      return response()->json(['status' => '1']);

    }

    public function onload(){
      date_default_timezone_set("Asia/Bangkok");
      $today = date('Y-m-d');
      $cek_data = Pendaftaran::all()->last();
      $rekap = Rekap::all()->last();
      if($today == $rekap['created_at']->format('Y-m-d')){
        $dipanggil = $rekap['dipanggil'];
      }else {
        $dipanggil = '0';
      }

      if($today == $cek_data['created_at']->format('Y-m-d')){
        if($dipanggil != '0'){
          $dataterakhir = Pendaftaran::where('nomor_antrian', '=',$dipanggil)->get()->last();
          return response()->json($dataterakhir);
        }else{
            return response()->json(['status' => '0']);
        }

      }else{
        return response()->json(['status' => '0']);
      }


    }

    public function get_all_pasien(){

      $data = Pasien::all();
      return response()->json($data);

    }

    public function reset(){

      $delete = Pendaftaran::whereDate('created_at', DB::raw('CURDATE()'))->delete();
      $resetrekap = Rekap::whereDate('created_at', DB::raw('CURDATE()'))->delete();
      $resetrekappoli = Rekapoli::whereDate('created_at', DB::raw('CURDATE()'))->delete();
      return response()->json(['status' => '1']);

    }



}
