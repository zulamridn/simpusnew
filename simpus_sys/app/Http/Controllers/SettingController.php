<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\Printer;
use App\TV;
use Illuminate\Support\Facades\DB;


class SettingController extends Controller {

  public function index(){
    $tv = TV::all();
    $data = Setting::all();

    return view('root.setting', compact('data', 'tv'));

  }

  public function edit_setting(Request $req){

    $data = Setting::where('id', $req->id)->update([
      'nama' => $req->nama,
      'alamat' => $req->alamat,
      'logo' => $req->logo,
      'back2' => $req->back2,
      'no_ambulance' => $req->no_ambulance,
      'batas_online' => $req->batas_online,
      'jam_awal' => $req->jam_awal,
      'jam_akhir' => $req->jam_akhir,
      'vidio' => $req->vidio,
      'text' => $req->text,
      'volume_none' => $req->volume_none,
      'volume_oncall' => $req->volume_oncall,
    ]);
    
    $tv = TV::where('id', '1')->update([
            'vidio' => $req->vidio,
            'text' => $req->text,
            'volume_none' => $req->volume_none,
            'volume_oncall' => $req->volume_oncall,
        ]);

    return redirect('/setting');
  }

  public function printsetting(Request $req){

    $loket = $req->loket;
    $app_key = $req->app_key;
    $app_port = $req->app_port;

    $setting = Printer::where('loket', $loket)->update([
      'app_key' => $app_key,
      'app_port' => $app_port,
    ]);

    return response()->json(['status' => '1']);

  }

}
