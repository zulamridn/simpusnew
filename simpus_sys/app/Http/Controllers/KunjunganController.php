<?php

namespace App\Http\Controllers;


use App\Rekap;
use App\Pendaftaran;
use App\Rekamedis;
use App\Onlinecount;
use App\Pelayanan;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class KunjunganController extends Controller
{

  public function list_hari(){

    $hari = DB::table('table_pendaftaran')
              ->select(DB::raw('date(table_pendaftaran.created_at) as date'))
              ->groupBy('date')
              ->get();

    return view('root.kunjungan_list_harian', compact('hari'));

  }

  public function rekap_harian(Request $req){

      $kunjungan = DB::table('table_pendaftaran')
                ->join('table_pelayanan_master', 'table_pelayanan_master.kode_antrian', '=', 'table_pendaftaran.kode_poli')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, count(table_pendaftaran.kode_poli) as poli'))
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->groupBy('table_pelayanan_master.nama_pelayanan')
                ->get();

      $bpjs = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.bpjs', '=', '1')
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->count();

      $umum = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.bpjs', '=', '0')
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->count();

      $pria = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.jenis_kelamin', '=', 'L')
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->count();

      $wanita = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.jenis_kelamin', '=', 'P')
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->count();

      $online = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.online', '=', '1')
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->count();

      $offline = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.online', '=', '0')
                ->whereDate('table_pendaftaran.created_at', '=', $req->hari)
                ->count();

      $filter = "Hari / Tanggal";
      $hari  = date('d F Y', strtotime($req->hari));

      return view('root.kunjungan_rekap', compact('kunjungan', 'hari', 'filter', 'bpjs', 'umum', 'pria', 'wanita', 'offline', 'online'));
  }

  public function list_bulan_tahun(){

    $bulan = DB::table('table_pendaftaran')
              ->select(DB::raw('month(table_pendaftaran.created_at) as month, year(table_pendaftaran.created_at) as year'))
              ->groupBy('month', 'year')
              ->get();

    return view('root.kunjungan_list_bulan', compact('bulan'));

  }

  public function rekap_bulanan(Request $req){

      $kunjungan = DB::table('table_pendaftaran')
                ->join('table_pelayanan_master', 'table_pelayanan_master.kode_antrian', '=', 'table_pendaftaran.kode_poli')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, count(table_pendaftaran.kode_poli) as poli'))
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->groupBy('table_pelayanan_master.nama_pelayanan')
                ->get();

      $bpjs = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.bpjs', '=', '1')
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $umum = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.bpjs', '=', '0')
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $pria = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.jenis_kelamin', '=', 'L')
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $wanita = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.jenis_kelamin', '=', 'P')
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $online = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.online', '=', '1')
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $offline = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.online', '=', '0')
                ->whereMonth('table_pendaftaran.created_at', '=', $req->bulan)
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $filter = "Bulan";
      $hari = date('F', strtotime($req->bulan));

      return view('root.kunjungan_rekap', compact('kunjungan', 'hari', 'filter', 'bpjs', 'umum', 'pria', 'wanita', 'offline', 'online'));
  }

  public function list_tahun(){

    $tahun = DB::table('table_pendaftaran')
              ->select(DB::raw('year(table_pendaftaran.created_at) as year'))
              ->groupBy('year')
              ->get();

    return view('root.kunjungan_list_tahun', compact('tahun'));

  }

  public function rekap_tahunan(Request $req){

      $kunjungan = DB::table('table_pendaftaran')
                ->join('table_pelayanan_master', 'table_pelayanan_master.kode_antrian', '=', 'table_pendaftaran.kode_poli')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, count(table_pendaftaran.kode_poli) as poli'))
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->groupBy('table_pelayanan_master.nama_pelayanan')
                ->get();

      $bpjs = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.bpjs', '=', '1')
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $umum = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.bpjs', '=', '0')
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $pria = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.jenis_kelamin', '=', 'L')
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $wanita = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.jenis_kelamin', '=', 'P')
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $online = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.online', '=', '1')
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $offline = DB::table('table_pendaftaran')
                ->where('table_pendaftaran.online', '=', '0')
                ->whereYear('table_pendaftaran.created_at', '=', $req->tahun)
                ->count();

      $filter = "Tahun";
      $hari = date('Y', strtotime($req->tahun));

      return view('root.kunjungan_rekap', compact('kunjungan', 'hari', 'filter', 'bpjs', 'umum', 'pria', 'wanita', 'offline', 'online'));
  }

}
