<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Download;

class DownloadController extends Controller{

  public function index(){

    $data = Download::orderby('id', 'DESC')->get();

    return view('root.download', compact('data'));

  }
}
