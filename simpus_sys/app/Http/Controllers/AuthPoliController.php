<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelayanan;
use App\Dokter;
use App\Kodeantrian;
use App\Users;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AuthPoliController extends Controller{


  public function Login(Request $req){

    $username = $req->username;
    $password = $req->password;

    //$data = Pelayanan::where('username', $username)->get()->last();
    $data = DB::table('table_pelayanan_master')
            ->join('table_dokter_master', 'table_dokter_master.id_dokter', '=', 'table_pelayanan_master.id_dokter')
            ->select('table_pelayanan_master.*', 'table_dokter_master.nama')
            ->where('table_pelayanan_master.username', '=', $username)
            ->limit('1')
            ->get()->last();

    $password_from_database = $data->password;


    //return response()->json();
    if(Hash::check($password, $data->password)){

      $req->session()->put('akses', 'poli');
      $req->session()->put('kode', $data->kode_antrian);
      $req->session()->put('poli', $data->nama_pelayanan);
      $req->session()->put('dokter', $data->nama);
      return response()->json(['status' => '1', 'akses' => Session::get('akses')]);

    }else{
      return response()->json(['status' => '0']);
    }

  }

  public function LoginAll(Request $req){

    $username = $req->username;
    $password = $req->password;

    $data = Users::where('username', $username)->get()->last();

    if(Hash::check($password, $data->password)){

      $req->session()->put('akses', $data->akses);
      $req->session()->put('username', $data->username);
      return response()->json(['status' => '1', 'akses' => Session::get('akses')]);

    }else{
      return response()->json(['status' => '0']);
    }

    //return response()->json($username);

  }



}
