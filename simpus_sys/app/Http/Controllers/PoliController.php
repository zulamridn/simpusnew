<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Onlinecount;
use App\Setting;
use App\Rekap;
use App\Pasien;
use App\Pelayanan;
use App\Rekapoli;
use App\Tarif;
use App\Diagnosa;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PoliController extends Controller{

  public function index(Request $req){

    if($req->session()->get('kode') != null){
      date_default_timezone_set("Asia/Bangkok");
      $kode = $req->session()->get('kode');
      $cekdatarekap = Rekapoli::where('kode_antrian_poli', $kode)->get()->last();
      if($cekdatarekap != null){
        $today = date('Y-m-d');
        $cektanggalterakhir = $cekdatarekap['created_at']->format('Y-m-d');
        if($today != $cektanggalterakhir){
          $defaultrekap = Rekapoli::create([
            'kode_antrian_poli' => $kode,
            'total' => '0',
            'dipanggil' => '0',
            'sisa' => '0'
          ]);
        }
        $data = Rekapoli::where('kode_antrian_poli', $kode)->get()->last();
        $tarif = Tarif::all();

        return view('root.poli', compact( 'data', 'tarif'));
        //return response()->json(Session::get('kodepoli'));
      }else{
        $defaultrekap = Rekapoli::create([
          'kode_antrian_poli' => $kode,
          'total' => '0',
          'dipanggil' => '0',
          'sisa' => '0'
        ]);
        $data = Rekapoli::where('kode_antrian_poli', $kode)->get()->last();
        $tarif = Tarif::all();
        return view('root.poli', compact( 'data', 'tarif'));
        //return response()->json(Session::get('kodepoli'));
      }

    }else{
      return view('poli');
    }
    //return response()->json($kode);

  }

  public function onloadpoli(Request $req){

    $last_antri = Rekapoli::whereDate('created_at', DB::RAW('CURDATE()'))->where('kode_antrian_poli', $req->kode_antrian)->get()->last();
    $get_data = DB::table('table_pendaftaran')
                ->where('kode_poli', '=', $req->kode_antrian)
                ->where('antrian_poli', '=', $last_antri->dipanggil)
                ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                ->get()
                ->last();

    $pasien = Pasien::where('nik', $get_data->id_pasien)->get()->last();
    
    if($last_antri->dipanggil === 0){
      $status = '0';
    }else{
      $status = '1';
    }

    return response()->json(['status' => $status, 'antrian' => $get_data, 'pasien' => $pasien, 'dipanggil' => $last_antri]);

  }

  public function panggil(Request $req){
    date_default_timezone_set("Asia/Bangkok");
    $today = date('Y-m-d');
    $kode = $req->session()->get('kode');
    $dipanggil = '1';
    $cek_data = Rekapoli::where('kode_antrian_poli', '=', $kode)->get()->last();
    if($today == $cek_data['created_at']->format('Y-m-d')){
      $ceksisa = Rekapoli::where('id', $cek_data['id'])->get()->last();
      if($ceksisa['sisa'] != '0'){
        $dipanggil = $cek_data['dipanggil']+1;
        $update_panggil = Rekapoli::where('id', $cek_data['id'])->update([
          'dipanggil' => $dipanggil,
          'sisa' => $cek_data['total']-$dipanggil
        ]);
        $data = DB::table('table_pendaftaran')
                ->join('table_rekap_poli', 'table_rekap_poli.kode_antrian_poli', '=', 'table_pendaftaran.kode_poli')
                //->join('table_rekap_poli', 'table_rekap_poli.dipanggil', '=', 'table_pendaftaran.antrian_poli')
                ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
                ->where('table_rekap_poli.kode_antrian_poli', '=', $kode)
                ->where('table_pendaftaran.antrian_poli', '=', $dipanggil)
                ->get();

        $kodelower = strtolower($kode);

        $content      = array(
            "en" => $kodelower.$dipanggil
        );
        $hashes_array = array();
        $fields = array(
            'app_id' => "74a559aa-2369-4239-8a4e-86dc48f190bc",
            'included_segments' => array(
                'All'
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic Y2Y3Y2Y3MGUtZTAxNi00M2E5LWIzM2EtMDI3OTJmYWFmMjc3'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return response()->json(['sisa' => $cek_data['total']-$dipanggil, 'dipanggil' =>  $dipanggil, 'status' => '1', 'data' => $data]);
      }else{
        return response()->json(['sisa' => '0']);
      }
    }else{
      return response()->json(['status' => '0']);
    }


  }

  public function ulang(Request $req){

    $kodeantrian = $req->kodeantrian;
    $dipanggil = $req->nomorantrian;

    $content      = array(
        "en" => $kodeantrian.$dipanggil
    );
    $hashes_array = array();
    $fields = array(
        'app_id' => "74a559aa-2369-4239-8a4e-86dc48f190bc",
        'included_segments' => array(
            'All'
        ),
        'data' => array(
            "foo" => "bar"
        ),
        'contents' => $content,
        'web_buttons' => $hashes_array
    );

    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic Y2Y3Y2Y3MGUtZTAxNi00M2E5LWIzM2EtMDI3OTJmYWFmMjc3'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return response()->json(['status' => '1']);

  }


}
