<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Pasien;
use App\Diagnosa;
use App\Rekamedis;
use App\ICD;
use App\Tarif;
use App\Setting;
use Illuminate\Support\Facades\DB;


class RekamController extends Controller
{
  public function index(Request $req){
    if($req->session()->get('akses') != null && $req->session()->get('akses') == 'rm' ){
        $data = DB::table('table_pendaftaran')
                ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
                ->where('table_pendaftaran.rm', '=', '0')
                ->where('table_pendaftaran.poli', '=', '1')
                ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                ->orderby('nomor_antrian', 'asc')
                ->get();

        return view('root.rekammedik', compact('data'));
    }else{
        return redirect('/loginall');
    }
  }

  public function create_rm(Request $req){
    $biodata = DB::table('table_pendaftaran')
              ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
              ->select('table_pasien.no_rm', 'table_pasien.nama', 'table_pendaftaran.*')
              ->where('table_pendaftaran.kode_antrian', '=', $req->register)
              ->get()->last();

    $diagnosa = Diagnosa::where('no_registrasi', $req->register)->get()->last();

    $icd = DB::table('table_icd')
            ->select('table_icd.*')
            ->get();
    //$diagnosa = Diagnosa::where('no_registrasi', $req->registasi)->get();
    return view('root.rekammedikinput', compact('icd', 'biodata', 'diagnosa'));

  }

  public function post_rm(Request $req){

    $icd10 = $req->icd10;
    $dataicd10 = explode(",", $icd10);
    $jlh = count($dataicd10);

    for($i = 0; $i<$jlh; $i++){

      $create = Rekamedis::create([
        'no_register' => $req->no_register,
        'no_rm' => $req->no_rm,
        'kode_rm' => $dataicd10[$i]
      ]);
    }

    $upate = Pendaftaran::where('kode_antrian', $req->no_register)->update([
      'rm' => '1'
    ]);

    return response()->json(['status' => '1']);

  }

  public function icd(){
    $icd = DB::table('table_icd')
            ->select('table_icd.*')
            ->get();

    return view('root.rekammedik_icd', compact('icd'));
  }

  public function icd_create(Request $req){
    $icd = ICD::create([
      'icd' => $req->icd,
      'nama_penyakit' => $req->nama_penyakit
    ]);

    return redirect('/icd_all');
  }

  public function icd_edit(Request $req){

    $data = ICD::where('icd', $req->icd)->get();

    return view('root.rekammedik_icd_edit', compact('data'));

  }

  public function icd_update(Request $req){

    $data = ICD::where('icd', $req->icd)->update([
      'nama_penyakit' => $req->nama_penyakit,
    ]);

    return redirect('/icd_all');

  }

  public function icd_hapus(Request $req){

    $data = ICD::where('icd', $req->icd)->delete();

    return redirect('/icd_all');

  }

  public function pasien(){

    $pasien = Pasien::all();

    return view('root.pasien', compact('pasien'));

  }

  public function pasien_riwayat_list(Request $req){

    $data = Pendaftaran::where('id_pasien', $req->nik)->get();

    return view('root.pasien_riwayat_list', compact('data'));

  }

  public function pasien_riwayat_list_detail(Request $req){

    date_default_timezone_set("Asia/Bangkok");

    $diagnosa = DB::table('table_diagnosa')
            ->join('table_pendaftaran', 'table_diagnosa.no_registrasi', '=', 'table_pendaftaran.kode_antrian')
            ->where('table_pendaftaran.kode_antrian', '=', $req->registrasi )
            ->select('table_pendaftaran.bpjs', 'table_pendaftaran.online', 'table_pendaftaran.nomor_poli','table_diagnosa.*')
            ->get()->last();

    $rm = DB::table('table_rekamedik')
          ->join('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
          ->join('table_pendaftaran', 'table_pendaftaran.kode_antrian', '=', 'table_rekamedik.no_register')
          ->select('table_icd.*')
          ->where('table_pendaftaran.kode_antrian', '=', $req->registrasi )
          ->get();

          $farmasi = DB::table('table_rekamobat')
                ->join('table_pendaftaran', 'table_pendaftaran.kode_antrian', '=', 'table_rekamobat.no_register')
                ->select('table_rekamobat.*')
                ->where('table_pendaftaran.kode_antrian', '=', $req->registrasi )
                ->get();


                $pasien = DB::table('table_pendaftaran')
                        ->join('table_pasien', 'table_pasien.nik', '=', 'table_pendaftaran.id_pasien')
                        ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                        ->where('table_pendaftaran.kode_antrian', '=', $req->registrasi)
                        ->select('table_pendaftaran.*', 'table_pasien.nama', 'table_pasien.alamat', 'table_pasien.no_rm', 'table_pasien.no_hp')
                        ->get()->last();

                $biaya = DB::table('table_tindakan')
                        ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                        ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                        ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
                        ->select('table_tindakan.*', 'table_tarif_master.nama_tindakan', 'table_tarif_master.tarif')
                        ->get();

                $biaya_total = DB::table('table_tindakan')
                        ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                        ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                        ->where('table_tindakan.no_registrasi', '=', $req->registrasi)
                        ->select('table_tindakan.*', 'table_tarif_master.nama_tindakan', 'table_tarif_master.tarif')
                        ->sum('table_tarif_master.tarif');

                $tarif = Tarif::all();
                $setting = Setting::get()->last();

    //return response()->json($diagnosa);
    if($diagnosa != null){
      return view('root.pasien_riwayat_list_detail', compact('diagnosa', 'rm', 'farmasi', 'pasien', 'biaya', 'biaya_total', 'tarif', 'setting'));
    }else{
      return view('root.data_belum_tersedia');
    }



  }

  public function rekap_hari_ini(){
      date_default_timezone_set("Asia/Bangkok");
      $penyakit = DB::table('table_rekamedik')
                ->leftjoin('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
                ->select(DB::raw('table_icd.nama_penyakit,table_rekamedik.kode_rm, count(table_rekamedik.kode_rm) as total'))
                ->whereDate('table_rekamedik.created_at', '=', DB::raw('CURDATE()'))
                //->whereMonth('table_rekamedik.created_at', '=', '1')
                //->whereYear('table_rekamedik.created_at', '=', '2019')
                ->groupBy('table_rekamedik.kode_rm', 'table_icd.nama_penyakit')
                ->orderBy('total', 'DESC')
                ->get();

    return view('root.rekammedik_rekap', compact('penyakit'));
    //return response()->json($penyakit);
  }

  public function list_hari(){

    $hari = DB::table('table_rekamedik')
              ->select(DB::raw('date(table_rekamedik.created_at) as date'))
              ->groupBy('date')
              ->get();

    return view('root.rekammedik_list_harian', compact('hari'));

  }

  public function list_bulan_tahun(){

    $bulantahun = DB::table('table_rekamedik')
              ->select(DB::raw('month(table_rekamedik.created_at) as month, year(table_rekamedik.created_at) as year'))
              ->groupBy('month', 'year')
              ->get();

    return view('root.rekammedik_list_bulan', compact('bulantahun'));

  }

  public function list_tahun(){

    $tahun = DB::table('table_rekamedik')
              ->select(DB::raw('year(table_rekamedik.created_at) as year'))
              ->groupBy('year')
              ->get();

    return view('root.rekammedik_list_tahun', compact('tahun'));

  }

  public function rekap_harian(Request $req){
      date_default_timezone_set("Asia/Bangkok");
      $penyakit = DB::table('table_rekamedik')
              ->leftjoin('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
              ->select(DB::raw('table_icd.nama_penyakit,table_rekamedik.kode_rm, count(table_rekamedik.kode_rm) as total'))
              ->whereDate('table_rekamedik.created_at', '=', $req->hari)
              ->groupBy('table_rekamedik.kode_rm', 'table_icd.nama_penyakit')
              ->orderBy('total', 'DESC')
              ->get();

      return view('root.rekammedik_rekap_bulanan', compact('penyakit'));
  }

  public function rekap_bulanan(Request $req){
      date_default_timezone_set("Asia/Bangkok");
      $penyakit = DB::table('table_rekamedik')
              ->leftjoin('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
              ->select(DB::raw('table_icd.nama_penyakit,table_rekamedik.kode_rm, count(table_rekamedik.kode_rm) as total'))
              //->whereDate('table_rekamedik.created_at', '=', DB::raw('CURDATE()'))
              ->whereMonth('table_rekamedik.created_at', '=', $req->bulan)
              ->whereYear('table_rekamedik.created_at', '=', $req->tahun)
              ->groupBy('table_rekamedik.kode_rm', 'table_icd.nama_penyakit')
              ->orderBy('total', 'DESC')
              ->get();

      return view('root.rekammedik_rekap_bulanan', compact('penyakit'));
  }

  public function rekap_tahunan(Request $req){
      date_default_timezone_set("Asia/Bangkok");
      $penyakit = DB::table('table_rekamedik')
              ->leftjoin('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
              ->select(DB::raw('table_icd.nama_penyakit,table_rekamedik.kode_rm, count(table_rekamedik.kode_rm) as total'))
              ->whereYear('table_rekamedik.created_at', '=', $req->tahun)
              ->groupBy('table_rekamedik.kode_rm', 'table_icd.nama_penyakit')
              ->orderBy('total', 'DESC')
              ->get();

      return view('root.rekammedik_rekap_bulanan', compact('penyakit'));
  }



}
