<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelayanan;
use App\Dokter;
use App\Kodeantrian;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PelayananController extends Controller
{
    public function view(){
      $dokter = Dokter::all();
      $kode = Kodeantrian::where('use', '0')->get();
      //$data = Pelayanan::all();
      $data = DB::table('table_pelayanan_master')
              ->join('table_dokter_master', 'table_dokter_master.id_dokter', '=', 'table_pelayanan_master.id_dokter')
              ->select('table_pelayanan_master.*', 'table_dokter_master.nama')
              ->get();

      return view('root.pelayanan_view', compact('data', 'dokter', 'kode'));
    }

    public function create(Request $req){

      $data = Pelayanan::create([
        'nama_pelayanan' => $req->nama_pelayanan,
        'id_dokter' => $req->id_dokter,
        'kode_antrian' => $req->kode_antrian,
        'username' => $req->username,
        'password' => Hash::make($req->password)
      ]);

      $antrian = Kodeantrian::where('kode', $req->kode_antrian )->update([
        'use' => '1'
      ]);

      return redirect('/pelayanan')->with('pesan', 'sukses');

    }

    public function update_link(Request $req){

      $data = Pelayanan::where('id', $req->id)->get();
      return view('root.dokter_edit', compact('data'));

    }

    public function update(Request $req){
      $data = Pelayanan::where('id', $req->id)->update([
        'nama' => $req->nama,
        'spesialis' =>$req->spesialis
      ]);

      return redirect('/dokter')->with('pesan', 'edit');
    }

    public function delete(Request $req){

      $data = Pelayanan::where('id', $req->id)->delete();
      $antrian = Kodeantrian::where('kode', $req->kode_antrian )->update([
        'use' => '0'
      ]);
      return redirect()->back()->with('pesan', 'sukses');

    }
}
