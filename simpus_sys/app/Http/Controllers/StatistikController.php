<?php

namespace App\Http\Controllers;


use App\Rekap;
use App\Pendaftaran;
use App\Rekamedis;
use App\Onlinecount;
use App\Pelayanan;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class StatistikController extends Controller
{

    public function harian(){
      date_default_timezone_set("Asia/Bangkok");
      $total = Rekap::whereDate('created_at', DB::raw('CURDATE()'))->get()->last();

      if($total != null){
        //return response()->json($data);
      }else{
        $insert_rekap = Rekap::create([
          'total_antrian' => '0',
          'dipanggil' => '0',
          'sisa' => '0',
        ]);
      }

      $laki = DB::table('table_pendaftaran')
              ->where('jenis_kelamin', '=', 'L')
              ->whereDate('created_at', '=', DB::raw('CURDATE()'))
              ->count();

      $perempuan = DB::table('table_pendaftaran')
              ->where('jenis_kelamin', '=', 'P')
              ->whereDate('created_at', '=', DB::raw('CURDATE()'))
              ->count();

      $bpjs = DB::table('table_pendaftaran')
              ->where('bpjs', '=', '1')
              ->where('id_pasien', '!=', 'NULL')
              ->whereDate('created_at', '=', DB::raw('CURDATE()'))
              ->count();

      $umum = DB::table('table_pendaftaran')
              ->where('bpjs', '=', '0')
              ->where('id_pasien', '!=', 'NULL')
              ->whereDate('created_at', '=', DB::raw('CURDATE()'))
              ->count();

      $offline = DB::table('table_pendaftaran')
              ->where('online', '=', '0')
              ->where('id_pasien', '!=', 'NULL')
              ->whereDate('created_at', '=', DB::raw('CURDATE()'))
              ->count();

      $online = DB::table('table_pendaftaran')
              ->where('online', '=', '1')
              ->where('id_pasien', '!=', 'NULL')
              ->whereDate('created_at', '=', DB::raw('CURDATE()'))
              ->count();

      $penyakit = DB::table('table_rekamedik')
                  ->leftjoin('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
                  ->select(DB::raw('table_icd.nama_penyakit,table_rekamedik.kode_rm, count(table_rekamedik.kode_rm) as total'))
                  ->whereDate('table_rekamedik.created_at', '=', DB::raw('CURDATE()'))
                  ->groupBy('table_rekamedik.kode_rm', 'table_icd.nama_penyakit')
                  ->orderBy('total', 'DESC')
                  ->limit(10)
                  ->get();



      $total_online = Onlinecount::whereDate('created_at', DB::raw('CURDATE()'))->get()->last();

      if($total_online != null){
        $online_today = $total_online->count;
      }else{
        $online_today = '0';
      }

      return view('root.statsharian', compact('total', 'laki', 'perempuan', 'bpjs', 'umum', 'penyakit', 'online_today', 'offline', 'online'));
      //return response()->json($penyakit);
    }

    public function bypoli(){

      $bypoli = DB::table('table_pendaftaran')
                ->join('table_pelayanan_master', 'table_pelayanan_master.kode_antrian', '=', 'table_pendaftaran.kode_poli')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, count(table_pendaftaran.kode_poli) as poli'))
                ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
                ->groupBy('table_pelayanan_master.nama_pelayanan')
                ->get();

      return response()->json($bypoli);

    }

    public function nilai_bypoli(){

      $tahun = date('Y');
      $bulan = date('m');

      $nilai = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->whereDate('table_nilai.created_at', '=', DB::raw('CURDATE()'))
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      $nilaisepanjangmasa = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      $nilaibulan = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->where(DB::raw('MONTH(table_nilai.created_at)'), '=', $bulan)
                ->where(DB::raw('YEAR(table_nilai.created_at)'), '=', $tahun)
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      $nilaitahun = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan,sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->where(DB::raw('YEAR(table_nilai.created_at)'), '=', $tahun)
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      //return response()->json($nilai);
      return view('root.statistik_nilai', compact('nilai', 'nilaibulan', 'nilaitahun', 'nilaisepanjangmasa'));

    }

    public function nilai_sepanjang_masa_json(){
      $nilaisepanjangmasa = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();
      return response()->json($nilaisepanjangmasa);
    }

    public function nilai_hari_ini_json(){
      $tahun = date('Y');
      $bulan = date('m');
      $nilai = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->whereDate('table_nilai.created_at', '=', DB::raw('CURDATE()'))
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      return response()->json($nilai);
    }

    public function nilai_bulan_ini_json(){
      $tahun = date('Y');
      $bulan = date('m');
      $nilaibulan = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan, sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->where(DB::raw('MONTH(table_nilai.created_at)'), '=', $bulan)
                ->where(DB::raw('YEAR(table_nilai.created_at)'), '=', $tahun)
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      return response()->json($nilaibulan);
    }

    public function nilai_tahun_ini_json(){
      $tahun = date('Y');
      $bulan = date('m');
      $nilaitahun = DB::table('table_nilai')
                ->join('table_pelayanan_master', 'table_pelayanan_master.id', '=', 'table_nilai.id_pelayanan')
                ->select(DB::raw('table_pelayanan_master.nama_pelayanan,sum(table_nilai.nilai) as nilai, count(table_nilai.id) as total'))
                ->where(DB::raw('YEAR(table_nilai.created_at)'), '=', $tahun)
                ->groupBy('table_nilai.id_pelayanan','table_pelayanan_master.nama_pelayanan')
                ->get();

      return response()->json($nilaitahun);

    }

    public function keuangan(){



      $rekaptanggal = DB::table('table_keuangan')
              ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('DATE(table_keuangan.created_at) as tanggal, MONTH(table_keuangan.created_at) as bulan, YEAR(table_keuangan.created_at) as tahun'))
              ->groupBy('tanggal', 'bulan', 'tahun')
              ->get();

      $rekapbulan = DB::table('table_keuangan')
              ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('MONTH(table_keuangan.created_at) as bulan, YEAR(table_keuangan.created_at) as tahun'))
              ->groupBy('bulan', 'tahun')
              ->get();

      $rekaptahun = DB::table('table_keuangan')
              ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('YEAR(table_keuangan.created_at) as tahun'))
              ->groupBy('tahun')
              ->get();

      return view('root.statistik_keuangan', compact('rekaptanggal', 'rekapbulan', 'rekaptahun'));

    }


    public function keuangan_json(){

      $rekap = DB::table('table_keuangan')
              ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('DATE(table_keuangan.created_at) as filter, MONTH(table_keuangan.created_at) as bulan, YEAR(table_keuangan.created_at) as tahun'))
              ->groupBy('filter', 'bulan', 'tahun')
              ->get();

      $filter = 'Tanggal';

      return response()->json($rekap);

    }

    public function bulanan_json(){

        $rekap = DB::table('table_keuangan')
                ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('MONTH(table_keuangan.created_at) as bulan,  YEAR(table_keuangan.created_at) as tahun'))
                ->groupBy('bulan', 'tahun')
                ->get();

        return response()->json($rekap);

    }

    public function tahunan_json(){

        $rekap = DB::table('table_keuangan')
                ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('YEAR(table_keuangan.created_at) as tahun'))
                ->groupBy('tahun')
                ->get();

        return response()->json($rekap);

    }

    public function kunjungan(){

      $tahun = date('Y');
      $bulan = date('m');

      $rekaptanggal = DB::table('table_pendaftaran')
              ->select(DB::raw('count(table_pendaftaran.id) as total') ,DB::raw('DATE(table_pendaftaran.created_at) as tanggal, MONTH(table_pendaftaran.created_at) as bulan, YEAR(table_pendaftaran.created_at) as tahun'))
              ->where(DB::raw('MONTH(table_pendaftaran.created_at)'), '=', $bulan)
              ->where(DB::raw('YEAR(table_pendaftaran.created_at)'), '=', $tahun)
              ->groupBy('tanggal', 'bulan', 'tahun')
              ->orderBy('tanggal', 'desc')
              ->get();

      $rekapbulan = DB::table('table_pendaftaran')
              ->select(DB::raw('count(table_pendaftaran.id) as total'), DB::raw('MONTH(table_pendaftaran.created_at) as bulan, YEAR(table_pendaftaran.created_at) as tahun'))
              ->where(DB::raw('YEAR(table_pendaftaran.created_at)'), '=', $tahun)
              ->groupBy( 'bulan', 'tahun')
              ->orderBy('bulan', 'desc')
              ->get();


      $rekaptahun = DB::table('table_pendaftaran')
              ->select(DB::raw('count(table_pendaftaran.id) as total'), DB::raw('YEAR(table_pendaftaran.created_at) as tahun'))
              ->groupBy( 'tahun')
              ->orderBy('tahun', 'desc')
              ->get();

      return view('root.statistik_kunjungan', compact('rekaptanggal', 'rekapbulan', 'rekaptahun'));

    }

    public function kunjungan_json(){
      $tahun = date('Y');
      $bulan = date('m');

      $rekaptanggal = DB::table('table_pendaftaran')
              ->select(DB::raw('count(table_pendaftaran.id) as total') ,DB::raw('DATE(table_pendaftaran.created_at) as tanggal, MONTH(table_pendaftaran.created_at) as bulan, YEAR(table_pendaftaran.created_at) as tahun'))
              ->where(DB::raw('MONTH(table_pendaftaran.created_at)'), '=', $bulan)
              ->where(DB::raw('YEAR(table_pendaftaran.created_at)'), '=', $tahun)
              ->groupBy('tanggal', 'bulan', 'tahun')
              ->orderBy('tanggal', 'desc')
              ->get();

      return response()->json($rekaptanggal);
    }

    public function kunjungan_bul_json(){
      $tahun = date('Y');
      $bulan = date('m');

      $rekapbulan = DB::table('table_pendaftaran')
              ->select(DB::raw('count(table_pendaftaran.id) as total'), DB::raw('MONTH(table_pendaftaran.created_at) as bulan, YEAR(table_pendaftaran.created_at) as tahun'))
              ->where(DB::raw('YEAR(table_pendaftaran.created_at)'), '=', $tahun)
              ->groupBy( 'bulan', 'tahun')
              ->orderBy('bulan', 'desc')
              ->get();

      return response()->json($rekapbulan);
    }

    public function kunjungan_tah_json(){
      $tahun = date('Y');
      $bulan = date('m');

      $rekaptahun = DB::table('table_pendaftaran')
              ->select(DB::raw('count(table_pendaftaran.id) as total'), DB::raw('YEAR(table_pendaftaran.created_at) as tahun'))
              ->groupBy( 'tahun')
              ->get();

      return response()->json($rekaptahun);
    }

}
