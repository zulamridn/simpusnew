<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Keuangan;
use App\Tarif;
use App\Tindakan;
use Illuminate\Support\Facades\DB;

class KeuanganController extends Controller
{

    public function index(){

        $total = Keuangan::whereDate('created_at', DB::raw('CURDATE()'))->sum('total');
        $hariini = DB::table('table_tindakan')
                  ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                  ->select('table_tarif_master.tarif as tarif',DB::raw('table_tarif_master.nama_tindakan as tindakan,count(table_tarif_master.nama_tindakan) as kasus, sum(table_tarif_master.tarif) as biaya'))
                  //->select('table_tarif_master.nama_tindakan')
                  ->whereDate('table_tindakan.created_at', '=', DB::raw('CURDATE()'))
                  ->groupBy('tindakan', 'tarif')
                  ->get();
        //$history = Keuangan::whereDate('created_at', DB::raw('CURDATE()'))->get();

        return view('root.keuangan_today', compact('hariini', 'total'));
        //return response()->json($hariini);

    }


    // $penyakit = DB::table('table_rekamedik')
    //           ->leftjoin('table_icd', 'table_icd.icd', '=', 'table_rekamedik.kode_rm')
    //           ->select(DB::raw('table_icd.nama_penyakit,table_rekamedik.kode_rm, count(table_rekamedik.kode_rm) as total'))
    //           ->whereDate('table_rekamedik.created_at', '=', DB::raw('CURDATE()'))
    //           //->whereMonth('table_rekamedik.created_at', '=', '1')
    //           //->whereYear('table_rekamedik.created_at', '=', '2019')
    //           ->groupBy('table_rekamedik.kode_rm', 'table_icd.nama_penyakit')
    //           ->orderBy('total', 'DESC')
    //           ->get();



    public function harian(){

        $rekap = DB::table('table_keuangan')
                ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('DATE(table_keuangan.created_at) as filter'))
                ->groupBy('filter')
                ->get();

        $filter = 'Tanggal';

        return view('root.keuangan_rekap', compact('rekap', 'filter'));

    }

    public function bulanan(){

        $rekap = DB::table('table_keuangan')
                ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('MONTH(table_keuangan.created_at) as bulan'), DB::raw('YEAR(table_keuangan.created_at) as tahun'))
                ->groupBy('bulan', 'tahun')
                ->get();

        $filter = 'Bulan';

        return view('root.keuangan_rekap', compact('rekap', 'filter'));

    }

    public function tahunan(){

        $rekap = DB::table('table_keuangan')
                ->select(DB::raw('sum(table_keuangan.total) as total'), DB::raw('YEAR(table_keuangan.created_at) as tahun'))
                ->groupBy('tahun')
                ->get();

        $filter = 'Tahun';

        return view('root.keuangan_rekap', compact('rekap', 'filter'));

    }

    public function rekap_harian(Request $req){

      $total = Keuangan::whereDate('created_at', $req->hari)->sum('total');
      $hariini = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->select('table_tarif_master.tarif as tarif',DB::raw('table_tarif_master.nama_tindakan as tindakan,count(table_tarif_master.nama_tindakan) as kasus, sum(table_tarif_master.tarif) as biaya'))
                //->select('table_tarif_master.nama_tindakan')
                ->whereDate('table_tindakan.created_at', '=', $req->hari)
                ->groupBy('tindakan', 'tarif')
                ->get();
      //$history = Keuangan::whereDate('created_at', DB::raw('CURDATE()'))->get();

      return view('root.keuangan_today', compact('hariini', 'total'));
    }

    public function rekap_bulanan(Request $req){

      $total = Keuangan::whereMonth('created_at', $req->bulan)->whereYear('created_at', $req->tahun)->sum('total');
      $hariini = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->select('table_tarif_master.tarif as tarif',DB::raw('table_tarif_master.nama_tindakan as tindakan,count(table_tarif_master.nama_tindakan) as kasus, sum(table_tarif_master.tarif) as biaya'))
                ->whereMonth('table_tindakan.created_at', '=', $req->bulan)
                ->whereYear('table_tindakan.created_at', '=', $req->tahun)
                ->groupBy('tindakan', 'tarif')
                ->get();
      //$history = Keuangan::whereDate('created_at', DB::raw('CURDATE()'))->get();

      return view('root.keuangan_today', compact('hariini', 'total'));
    }

    public function rekap_tahunan(Request $req){

      $total = Keuangan::whereYear('created_at', $req->tahun)->sum('total');
      $hariini = DB::table('table_tindakan')
                ->join('table_tarif_master', 'table_tarif_master.id', '=', 'table_tindakan.id_tindakan')
                ->select('table_tarif_master.tarif as tarif',DB::raw('table_tarif_master.nama_tindakan as tindakan,count(table_tarif_master.nama_tindakan) as kasus, sum(table_tarif_master.tarif) as biaya'))
                ->whereYear('table_tindakan.created_at', '=', $req->tahun)
                ->groupBy('tindakan', 'tarif')
                ->get();
      //$history = Keuangan::whereDate('created_at', DB::raw('CURDATE()'))->get();

      return view('root.keuangan_today', compact('hariini', 'total'));
    }

}
