<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Diagnosa;
use App\Rekamobat;
use App\Obat;
use Illuminate\Support\Facades\DB;

class ApotikController extends Controller
{

  public function index(){

    $data = DB::table('table_pendaftaran')
            ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
            ->where('table_pendaftaran.apotik', '=', '0')
            ->whereDate('table_pendaftaran.created_at', '=', DB::raw('CURDATE()'))
            ->orderby('nomor_antrian', 'asc')
            ->get();

    return view('root.apotik', compact('data'));


  }

  public function create_apt(Request $req){
    $biodata = DB::table('table_pendaftaran')
              ->join('table_pasien', 'table_pasien.no_rm', '=', 'table_pendaftaran.id_pasien')
              ->select('table_pasien.no_rm', 'table_pasien.nama', 'table_pendaftaran.*')
              ->where('table_pendaftaran.kode_antrian', '=', $req->register)
              ->get()->last();

    $diagnosa = Diagnosa::where('no_registrasi', $req->register)->get()->last();

    $obat= DB::table('table_obat')
            ->select('table_obat.*')
            ->get();
    //$diagnosa = Diagnosa::where('no_registrasi', $req->registasi)->get();
    return view('root.apotikinput', compact('obat', 'biodata', 'diagnosa'));

  }

  public function create_obat(Request $req){
    $data = Obat::create([
      'nama_obat' => $req->nama_obat
    ]);

    return redirect('obat_all');
  }

  public function obat_link(Request $req){

    $data = Obat::where('id', $req->id)->get();

    return view('root.apotik_obat_edit', compact('data'));

  }

  public function obat_update(Request $req){

    $data = Obat::where('id', $req->id)->update([
      'nama_obat' => $req->nama_obat
    ]);

   return redirect('obat_all');

  }

  public function obat_hapus(Request $req){

    $data = Obat::where('id', $req->id)->delete();

   return redirect('obat_all');

  }

  public function index_obat(){
    $obat = Obat::all();
    return view('root.apotik_obat',compact('obat'));
  }

  public function post_obat(Request $req){

    $dataobat = $req->dataobat;
    $dataobatarray = explode(",", $dataobat);
    $jlh = count($dataobatarray);

    for($i = 0; $i<$jlh; $i++){

      $create = Rekamobat::create([
        'no_register' => $req->no_register,
        'no_rm' => $req->no_rm,
        'nama_obat' => $dataobatarray[$i]
      ]);
    }

    $upate = Pendaftaran::where('kode_antrian', $req->no_register)->update([
      'apotik' => '1'
    ]);

    return response()->json(['status' => '1']);

  }


  // Statistik ===============================================================================================================

  public function rekap_hari_ini(){
      date_default_timezone_set("Asia/Bangkok");
      $obat = DB::table('table_rekamobat')
                ->select(DB::raw('table_rekamobat.nama_obat, count(table_rekamobat.nama_obat) as total'))
                ->whereDate('table_rekamobat.created_at', '=', DB::raw('CURDATE()'))
                ->groupBy('table_rekamobat.nama_obat')
                ->orderBy('total', 'DESC')
                ->get();

    return view('root.apotik_rekap', compact('obat'));
    //return response()->json($penyakit);
  }


  public function list_hari(){

    $hari = DB::table('table_rekamobat')
              ->select(DB::raw('date(table_rekamobat.created_at) as date'))
              ->groupBy('date')
              ->get();

    return view('root.apotik_list_harian', compact('hari'));

  }

  public function list_bulan_tahun(){

    $bulantahun = DB::table('table_rekamobat')
              ->select(DB::raw('month(table_rekamobat.created_at) as month, year(table_rekamobat.created_at) as year'))
              ->groupBy('month', 'year')
              ->get();

    return view('root.apotik_list_bulan', compact('bulantahun'));

  }

  public function list_tahun(){

    $tahun = DB::table('table_rekamobat')
              ->select(DB::raw('year(table_rekamobat.created_at) as year'))
              ->groupBy('year')
              ->get();

    return view('root.apotik_list_tahun', compact('tahun'));

  }

  public function rekap_harian(Request $req){
      date_default_timezone_set("Asia/Bangkok");
      $obat = DB::table('table_rekamobat')
              ->select(DB::raw('table_rekamobat.nama_obat, count(table_rekamobat.nama_obat) as total'))
              ->whereDate('table_rekamobat.created_at', '=', $req->hari)
              ->groupBy('table_rekamobat.nama_obat' )
              ->orderBy('total', 'DESC')
              ->get();

      return view('root.apotik_rekap_bulanan', compact('obat'));
  }

  public function rekap_bulanan(Request $req){
      date_default_timezone_set("Asia/Bangkok");
      $obat = DB::table('table_rekamobat')
              ->select(DB::raw('table_rekamobat.nama_obat, count(table_rekamobat.nama_obat) as total'))
              ->whereMonth('table_rekamobat.created_at', '=', $req->bulan)
              ->whereYear('table_rekamobat.created_at', '=', $req->tahun)
              ->groupBy('table_rekamobat.nama_obat')
              ->orderBy('total', 'DESC')
              ->get();

      return view('root.apotik_rekap_bulanan', compact('obat'));
  }

  public function rekap_tahunan(Request $req){
      date_default_timezone_set("Asia/Bangkok");
      $obat = DB::table('table_rekamobat')
              ->select(DB::raw('table_rekamobat.nama_obat, count(table_rekamobat.nama_obat) as total'))
              ->whereYear('table_rekamobat.created_at', '=', $req->tahun)
              ->groupBy('table_rekamobat.nama_obat')
              ->orderBy('total', 'DESC')
              ->get();

      return view('root.rekammedik_rekap_bulanan', compact('obat'));
  }


}
