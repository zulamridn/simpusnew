<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
  protected $table = 'table_pendaftaran';
  protected $fillable = [ 'nomor_antrian', 'kode_antrian', 'online', 'bpjs', 'id_pasien','apotik', 'kasir', 'rm'];
}
