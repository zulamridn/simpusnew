<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biaya extends Model
{
  protected $table = 'table_biaya';
  protected $fillable = [ 'nomor_registrasi','nama_tindakan', 'tarif'];
}
