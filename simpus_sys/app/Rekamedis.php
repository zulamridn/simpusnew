<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekamedis extends Model
{
  protected $table = 'table_rekamedik';
  protected $fillable = ['no_register', 'no_rm', 'kode_rm'];
}
