<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/pendaftaran', 'PendaftaranController@index' );
Route::get('/loginall','LoginController@index');
Route::get('/register', 'LoginController@register');
Route::get('/ambilnomor', function(){
  return view('ambilnomor');
});

Route::get('/', function(){
  return view('landing');
});

Route::get('/print', 'PrintController@index');

Route::get('/logout', function(){
  Session::flush();
  return view('landing');
});

// ================== Dokter ===================== //
Route::get('/dokter', 'DokterController@view');
Route::post('/tambah_dokter', 'DokterController@create');
Route::get('/hapus/{id}', 'DokterController@delete');
Route::get('/edit/{id}', 'DokterController@update_link');
Route::post('/update', 'DokterController@update');

// ================== Master Pelayanan ============== //
Route::get('/pelayanan', 'PelayananController@view');
Route::post('/tambah_pelayanan', 'PelayananController@create');
Route::get('/hapus_pelayanan/{id}/{kode_antrian}', 'PelayananController@delete');
// Route::get('/edit/{id}', 'DokterController@update_link');
// Route::post('/update', 'DokterController@update');

// ================== Tarif ============== //
Route::get('/tarif', 'TarifController@view');
Route::post('/tambah_tarif', 'TarifController@create');
Route::get('/hapus_tarif/{id}', 'TarifController@delete');
Route::get('/edit_tarif/{id}', 'TarifController@update_link');
Route::post('/update_tarif', 'TarifController@update');


// ================== PASIEN ============== //
Route::post('/createpasien', 'PasienController@create_pasien');
Route::get('/edit_pasien/{rm}', 'PasienController@edit_pasien');
Route::post('/update_pasien', 'PasienController@update_pasien');

//==================== POLI ================//
Route::get('/logpoli', function(){
    return view('poli');
});


Route::get('/indexpoli', 'PoliController@index');


//=================== Apotek ==================//

Route::get('/indexapotik', 'ApotikController@index');
Route::get('/apotikinput/{id}/{register}', 'ApotikController@create_apt');
Route::get('/obat_all', 'ApotikController@index_obat');
Route::post('/obat_create', 'ApotikController@create_obat');
Route::get('/edit_obat/{id}', 'ApotikController@obat_link');
Route::post('/update_obat', 'ApotikController@obat_update');
Route::get('/hapus_obat/{id}', 'ApotikController@obat_hapus');

Route::get('/rekap_hari_ini_apt', 'ApotikController@rekap_hari_ini');
Route::get('/rekap_list_bulan_apt', 'ApotikController@list_bulan_tahun');
Route::get('/rekap_bulanan_apt/{bulan}/{tahun}', 'ApotikController@rekap_bulanan');
Route::get('/rekap_list_tahun_apt', 'ApotikController@list_tahun');
Route::get('/rekap_tahunan_apt/{tahun}', 'ApotikController@rekap_tahunan');
Route::get('/rekap_list_hari_apt', 'ApotikController@list_hari');
Route::get('/rekap_harian_apt/{hari}', 'ApotikController@rekap_harian');

//=================== Rekam Medis ==================//

Route::get('/indexrekamedis', 'RekamController@index');
Route::get('/rminput/{id}/{register}', 'RekamController@create_rm');
Route::get('/icd_all', 'RekamController@icd');
Route::post('/icd_create', 'RekamController@icd_create');
Route::get('/edit_icd/{icd}', 'RekamController@icd_edit');
Route::post('/update_icd', 'RekamController@icd_update');
Route::get('/hapus_icd/{icd}', 'RekamController@icd_hapus');

Route::get('/pasien_list', 'RekamController@pasien');
Route::get('/pasien_riwayat_list/{nik}', 'RekamController@pasien_riwayat_list');
Route::get('/pasien_riwayat_list_detail/{registrasi}', 'RekamController@pasien_riwayat_list_detail');

Route::get('/rekap_hari_ini', 'RekamController@rekap_hari_ini');
Route::get('/rekap_list_bulan', 'RekamController@list_bulan_tahun');
Route::get('/rekap_bulanan_rm/{bulan}/{tahun}', 'RekamController@rekap_bulanan');
Route::get('/rekap_list_tahun', 'RekamController@list_tahun');
Route::get('/rekap_tahunan_rm/{tahun}', 'RekamController@rekap_tahunan');
Route::get('/rekap_list_hari', 'RekamController@list_hari');
Route::get('/rekap_harian_rm/{hari}', 'RekamController@rekap_harian');

//==================== Statistik ====================//

Route::get('/statistik', 'StatistikController@harian');
Route::get('/statistik_keu', 'StatistikController@keuangan');
Route::get('/statistik_kunj', 'StatistikController@kunjungan');
Route::get('/statistik_nilai', 'StatistikController@nilai_bypoli');


// ==================== Kasir ====================//
Route::get('/kasir', 'KasirController@index');
Route::get('/kasirdetail/{registrasi}', 'KasirController@get_detail');
Route::get('/invoice/{registrasi}', 'KasirController@invoice');

// ====================== Setting ========================= //
Route::get('/setting', 'SettingController@index');
Route::post('/edit_setting', 'SettingController@edit_setting');

//========================= Download ======================== //
Route::get('/download', 'DownloadController@index');

// ======================= Keuangan ====================== //
Route::get('/indexkeuangan', 'KeuanganController@index');
Route::get('/rekap_harian', 'KeuanganController@harian');
Route::get('/rekap_bulanan', 'KeuanganController@bulanan');
Route::get('/rekap_tahunan', 'KeuanganController@tahunan');

Route::get('/rekap_bulanan_keu/{bulan}/{tahun}', 'KeuanganController@rekap_bulanan');
Route::get('/rekap_tahunan_keu/{tahun}', 'KeuanganController@rekap_tahunan');
Route::get('/rekap_harian_keu/{hari}', 'KeuanganController@rekap_harian');


// ======================== Kunjungan ======================= //
//Route::get('/rekap_hari_ini', 'RekamController@rekap_hari_ini');
Route::get('/rekap_list_bulan_kunj', 'KunjunganController@list_bulan_tahun');
Route::get('/rekap_bulanan_kunj/{bulan}/{tahun}', 'KunjunganController@rekap_bulanan');
Route::get('/rekap_list_tahun_kunj', 'KunjunganController@list_tahun');
Route::get('/rekap_tahunan_kunj/{tahun}', 'KunjunganController@rekap_tahunan');
Route::get('/rekap_list_hari_kunj', 'KunjunganController@list_hari');
Route::get('/rekap_harian_kunj/{hari}', 'KunjunganController@rekap_harian');
