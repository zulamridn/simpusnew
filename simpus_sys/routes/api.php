<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/getnomor', 'PendaftaranController@get_antrian');
Route::get('/getnomor/{online}/{bpjs}', 'PendaftaranController@get_antrian');
Route::get('/indexpendaftaran', 'PendaftaranController@indexapi');
Route::get('/panggil', 'PendaftaranController@panggil');
Route::get('/panggilpoli', 'PoliController@panggil');
Route::get('/onload', 'PendaftaranController@onload');
Route::get('/koneksi', 'PendaftaranController@tes_koneksi');
Route::get('/pasien', 'PendaftaranController@get_all_pasien');
Route::get('/pasien', 'PendaftaranController@get_all_pasien');
//Route::get('/create_pasien/{nik}/{nama}/{jenis_kelamin}/{tempat_lahir}/{tanggal_lahir}/{nama_kepala_keluarga}/{status_keluarga}/{no_hp}/{alamat}', 'PasienController@create_pasien');
Route::post('/create_pasien', 'PasienController@create_pasien');
Route::get('/getpasien/{nik}', 'PasienController@get_pasien_data');
Route::get('/getpasienrm/{rm}', 'PasienController@get_pasien_datarm');
Route::get('/antrianpoli/{id_antrian}/{id_pasien}/{kode_antrian}', 'PasienController@pendaftaran_poli');
Route::get('/pendaftaran', 'PendaftaranController@index' );

Route::get('/tesdata', 'PoliController@index');

//Realtime

Route::get('/data', 'RealTimeController@get_name_poli_antrian');
Route::get('/onesignal', 'PendaftaranController@onesignal');
Route::get('/onloadpoli/{kode_antrian}','PoliController@onloadpoli');

Route::get('/simpandatapoli/{registrasi}/{rm}/{nik}/{nama_poli}/{nama_dokter}/{keluhan}/{diagnosa}/{catatan}/{tindakan}', 'PasienController@create_diagnosa');

Route::group(['middleware' => 'api'], function () {
  Route::get('/poliindex', 'PoliController@index');
  Route::post('/loginpoli', 'AuthPoliController@Login');

});

Route::get('/loginall/{username}/{password}', 'AuthPoliController@LoginAll');
Route::get('/createuser/{username}/{password}/{akses}', 'UserController@create_user');

Route::get('/postrm/{icd10}/{no_register}/{no_rm}', 'RekamController@post_rm');
Route::get('/postapt/{dataobat}/{no_register}/{no_rm}', 'ApotikController@post_obat');


Route::get('/tv', 'TVController@getall');
Route::get('/reset', 'PendaftaranController@reset');
//Route::post('/create_pasien', 'PasienController@create_pasien');
Route::get('/ulangpanggil/{kode}/{nomorantrian}', 'PendaftaranController@ulang');
Route::get('/ulangpanggilpoli/{kodeantrian}/{nomorantrian}', 'PoliController@ulang');

Route::get('/create_android/{nik}/{nama}/{email}/{no_hp}/{alamat}', 'AndroidController@createuser');

Route::get('/tabletindex', 'TabletController@getData');
Route::get('/penilaian/{id}', 'TabletController@getDetail');
Route::get('/beripenilaian/{nilai}/{id}', 'TabletController@giveNilai');

//Android
Route::get('/getantrian', 'AndroidController@getAntrian');
Route::get('/getmy/{id}', 'AndroidController@getMy');
Route::get('/getantrianpoli', 'AndroidController@getAntianPoli');
Route::get('/nomorambulance', 'AndroidController@getNoAmbulace');

//statistik
Route::get('/statistik_keu_json', 'StatistikController@keuangan_json');
Route::get('/statistik_keu_json_bul', 'StatistikController@bulanan_json');
Route::get('/statistik_keu_json_tah', 'StatistikController@bulanan_json');

Route::get('/statistik_kunj_json', 'StatistikController@kunjungan_json');
Route::get('/statistik_kunj_json_bul', 'StatistikController@kunjungan_bul_json');
Route::get('/statistik_kunj_json_tah', 'StatistikController@kunjungan_tah_json');

Route::get('/statistik_bypoli', 'StatistikController@bypoli');

Route::get('/statistik_nilai_alltime', 'StatistikController@nilai_sepanjang_masa_json');
Route::get('/statistik_nilai_hari_ini', 'StatistikController@nilai_hari_ini_json');
Route::get('/statistik_nilai_bulan_ini', 'StatistikController@nilai_bulan_ini_json');
Route::get('/statistik_nilai_tahun_ini', 'StatistikController@nilai_tahun_ini_json');

//setprint
Route::get('/setprint/{loket}/{app_key}/{app_port}', 'SettingController@printsetting');

//kasir
Route::get('/tes_kwitansi/{registrasi}', 'KasirController@get_detail');
Route::get('/tambah_biaya/{registrasi}/{tindakan}', 'KasirController@tambah_biaya');
Route::get('/hapus_biaya/{id}', 'KasirController@hapus_biaya');
Route::get('/rekap_harian', 'KeuanganController@index');
Route::get('/simpan_biaya/{registrasi}/{total}', 'KasirController@simpan_biaya');

//rm
Route::get('/pasien_riwayat_list_detail/{registrasi}', 'RekamController@pasien_riwayat_list');
