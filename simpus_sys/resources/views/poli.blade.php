<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.png">
    <title>Ruang Pelayanan</title>
    <link href="/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <link href="/dist/css/style.min.css" rel="stylesheet">
</head>

<body class="skin-blue card-no-border">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Puskesmas Tebas</p>
        </div>
    </div>
    <section id="wrapper">
        <div class="login-register" >
            <div class="login-box card">
                <div class="card-body">
                    <center><img src="https://4.bp.blogspot.com/-lBQfphxJ4NI/WjFdnbCBhEI/AAAAAAAAECg/74ZtmQxr8jckDyypvWEGJdwSI77XrzXCwCLcBGAs/w1200-h630-p-k-no-nu/Logo%2BPuskesmas%2BTanpa%2BBackground.png" class="img-responsive" style="width:30%;margin-bottom:10px"/></center>
                    <form class="form-horizontal form-material" id="loginform">
                      @csrf
                        <center><h5 class="box-title m-b-20"></h5></center>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required placeholder="Username" name="username" id="username"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required placeholder="Password" name="password" id="password"> </div>
                        </div>
                    </form>

                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn btn-block btn-lg btn-info" id="login">Log In</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/assets/node_modules/popper/popper.min.js"></script>
    <script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ==============================================================
        // Login and Recover Password
        // ==============================================================
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });

        $('#login').click(function(){
          let username = $('#username').val();
          let password = $('#password').val();
          let form = $('#loginform').serialize();
          $.ajax({
            type: "POST",
            url: "/api/loginpoli",
            data : form,
            success: function(data){
              if(data.status === '1'){
                window.location.href = '/indexpoli';
              }else{
                alert("Password / Username Salah");
              }
              //console.log(data);
            }
          })

        })
    </script>

</body>

</html>
