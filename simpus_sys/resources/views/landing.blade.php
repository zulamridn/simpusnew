<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sistem Infromasi Puskesmas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;

            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                display: inline;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .box{
              padding: 10px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="container content">

                <div class="row">
                  <div class="col-lg-3">
                    <a href="/loginall">
                      <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                        <div class="card-header">Pendaftaran</div>
                        <div class="card-body">
                            <img src="/icon/png/computer.png" alt="" class="img-responsive" width="80%">
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a href="/logpoli">
                      <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                        <div class="card-header">Pelayanan</div>
                        <div class="card-body">
                            <img src="/icon/png/nurse-1.png" alt="" class="img-responsive" width="80%">
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a href="/loginall">
                      <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
                        <div class="card-header">Rekam Medis</div>
                        <div class="card-body">
                            <img src="/icon/png/folder.png" alt="" class="img-responsive" width="80%">
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a href="/loginall">
                      <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
                        <div class="card-header">Apotik</div>
                        <div class="card-body">
                            <img src="/icon/png/tablets.png" alt="" class="img-responsive" width="80%">
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a href="/loginall">
                      <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
                        <div class="card-header">Kasir</div>
                        <div class="card-body">
                            <img src="/icon/png/report.png" alt="" class="img-responsive" width="80%">
                        </div>
                      </div>
                    </a>
                  </div>
                  <div class="col-lg-3">
                    <a href="/loginall">
                      <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                        <div class="card-header">Statistik dan Laporan</div>
                        <div class="card-body">
                            <img src="/icon/png/hospital-2.png" alt="" class="img-responsive" width="80%">
                        </div>
                      </div>
                    </a>
                  </div>

                </div>
            </div>
        </div>


        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
