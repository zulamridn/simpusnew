@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Master ICD X</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Master ICD X</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#myModal" class="model_img img-responsive"><i class="fa fa-plus-circle"></i> Tambah Data</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data ICD X</h4>

                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>Kode ICD X</th>
                                                <th>Penyakit</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>No </th>
                                              <th>Kode ICD X</th>
                                              <th>Penyakit</th>
                                              <th>#</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($icd as $icd)
                                                <tr>
                                                  <td style="width:30px"> {{ $no++ }}   </td>
                                                  <td> {{ $icd->icd }} </td>
                                                  <td> {{ $icd->nama_penyakit }} </td>
                                                  <td style="width:120px; text-align:center"> <a href='/simpus/edit_icd/{{$icd->icd}}' class="btn btn-info" style="color:white"><i class="fa fa-pencil"></i>&nbsp&nbsp Edit</a> <a href="/simpus/hapus_icd/{{$icd->icd}}" class="btn btn-danger" style="color:white"><i class="fa fa-trash"></i>&nbsp&nbsp Hapus</a></td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Tambah ICD X</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form method="post" action="/simpus/icd_create">
                                              @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Nama Penyakit</label>
                                                  <input type="text" class="form-control" name="nama_penyakit" placeholder="Nama Penyakit" required>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Kode ICD X</label>
                                                  <input type="text" class="form-control" name="icd" placeholder="A00.1" required>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
