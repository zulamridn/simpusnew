@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Rekam Medis</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Data Pasien</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>NIK</th>
                                                <th>No RM</th>
                                                <th>Nama Pasien</th>
                                                <th>Jenis Kelamin</th>
                                                <th>TTL</th>
                                                <th>Alamat</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <tr>
                                                  <th>No </th>
                                                  <th>NIK</th>
                                                  <th>No RM</th>
                                                  <th>Nama Pasien</th>
                                                  <th>Jenis Kelamin</th>
                                                  <th>TTL</th>
                                                  <th>Alamat</th>
                                                  <th>#</th>
                                              </tr>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($pasien as $pasien)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $pasien->nik }} </td>
                                                  <td> {{ $pasien->no_rm }} </td>
                                                  <td> {{ $pasien->nama}} </td>
                                                  <td> {{ $pasien->jenis_kelamin }} </td>
                                                  <td> {{ $pasien->tempat_lahir }}, {{  date('d F Y', strtotime($pasien->tanggal_lahir))  }}</td>
                                                  <td> {{ $pasien->alamat }} </td>
                                                  <td> <center><a href='/simpus/pasien_riwayat_list/{{ $pasien->nik }}' class="btn btn-info" style="color:white"><i class="fa fa-eye"></i> Lihat Riwayat Pasien</a> <a href='/simpus/edit_pasien/{{ $pasien->no_rm }}' class="btn btn-success" style="color:white"><i class="fa fa-pencil"></i> Edit Data Pasien</a> </center></td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Tambah Data Dokter</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form method="post" action="/tambah_dokter">
                                              @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">ID Dokter</label>
                                                  <input type="text" class="form-control" name="id_dokter" aria-describedby="emailHelp" placeholder="ID Dokter">
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Nama Dokter</label>
                                                  <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" placeholder="Nama Lengkap dan Gelar">
                                                  <small id="emailHelp" class="form-text text-muted">Masukan nama lengkap dan gelar</small>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputPassword1">Spesialis</label>
                                                  <input type="text" class="form-control" name="spesialis" placeholder="Spesialis">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
