@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Rekam Medis</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Data Pasien</li>
                            </ol>
                        </div>
                    </div>
                </div>

              <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                      <div class="card-header">
                        <h4>Data Ruang Pelayanan</h4>
                      </div>
                      <div class="card-body">
                        <h5><b>{{ $diagnosa->nama_poli }}</b></h5>
                        <hr>
                        <p><i class="fa fa-stethoscope"></i>&nbsp&nbsp&nbsp&nbsp{{$diagnosa->nomor_poli}}</p>
                        <p><i class="fa fa-user-md"></i>&nbsp&nbsp&nbsp&nbsp{{ $diagnosa->nama_dokter }}</p>
                        <p><i class="fa fa-credit-card-alt"></i>&nbsp&nbsp&nbsp&nbsp @if($diagnosa->bpjs === '0') UMUM  @else BPJS @endif</p>
                        <p><i class="fa fa-mobile"></i>&nbsp&nbsp&nbsp&nbsp @if($diagnosa->online === '0') OFFLINE  @else ONLINE @endif</p>
                      </div>
                    </div>
                </div>

                <div class="col-lg-3">
                  <div class="card">
                    <div class="card-header">
                      <h4>Diagnosa</h4>
                    </div>
                    <div class="card-body">
                      <h5><b>Keluhan</b></h5>
                      <hr>
                      <p>{{$diagnosa->keluhan}}</p>
                      <hr>
                      <h5><b>Diagnosa</b></h5>
                      <hr>
                      <p>{{$diagnosa->diagnosa}}</p>
                      <hr>
                      <h5><b>Catatan Dokter</b></h5>
                      <hr>
                      <p>{{$diagnosa->catatan}}</p>
                      <hr>
                    </div>
                  </div>
                </div>

                <div class="col-lg-3">
                  <div class="card">
                    <div class="card-header">
                      <h4>Rekam Medis</h4>
                    </div>
                    <div class="card-body">
                      @foreach ($rm as $rm)
                        <h6><b>{{$rm->nama_penyakit}} - {{$rm->icd}}</b></h6>
                        <hr>
                      @endforeach
                    </div>
                  </div>
                </div>

                <div class="col-lg-3">
                  <div class="card">
                    <div class="card-header">
                      <h4>Farmasi</h4>
                    </div>
                    <div class="card-body">
                      @foreach ($farmasi as $farmasi)
                        <h6><b>{{$farmasi->nama_obat}}</b></h6>
                        <hr>
                      @endforeach
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">
                                  <div class="col-md-12">
                                      <div class="card" id="invoice">
                                          <div class="card-body">
                                              <h3><b>INVOICE</b> <span class="pull-right">#{{$pasien->kode_antrian}}</span></h3>
                                              <hr>
                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <div class="pull-left">
                                                          <address>
                                                              <h3> &nbsp;<b class="text-danger">{{$setting->nama}}</b></h3>
                                                              <p class="text-muted m-l-5">{{ $setting->alamat }}</p>
                                                          </address>
                                                      </div>
                                                      <div class="pull-right text-right">
                                                          <address>
                                                              <h3>Kepada,</h3>
                                                              <h4 class="font-bold">{{$pasien->nama}}</h4>
                                                              <p class="text-muted m-l-30">{{ $pasien->alamat }}</p>
                                                              <p class="text-muted m-l-5">{{ $pasien->no_hp }}</p>
                                                              <p class="m-t-30"><b>Tanggal :</b> {{ date(' d M Y')  }}</p>
                                                          </address>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                      <div class="table-responsive m-t-40">
                                                          <table class="table table-hover">
                                                              <thead>
                                                                  <tr>
                                                                      <th class="text-center">#</th>
                                                                      <th>Nama Tindakan</th>
                                                                      <th class="text-right">Biaya</th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody>
                                                              <?php  $no = 1 ?>
                                                              @foreach ($biaya as $biaya)
                                                                  <tr>
                                                                      <td class="text-center">{{ $no++ }}</td>
                                                                      <td>{{$biaya->nama_tindakan}}</td>
                                                                      <td class="text-right">{{ number_format($biaya->tarif,0,',','.') }}</td>
                                                                  </tr>
                                                                  <tr>
                                                              @endforeach
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-12">
                                                      <div class="pull-right m-t-30 text-right">
                                                          <h3><b>Total :</b> {{ number_format($biaya_total,0,',','.') }}</h3> </div>
                                                          <input type="text" id="total_biaya" value="{{ $biaya_total }}" hidden>
                                                      <div class="clearfix"></div>
                                                      <hr>
                                                        <h3><b>INVOICE</b> <span class="pull-right">#{{$pasien->kode_antrian}}</span></h3>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

@endsection
