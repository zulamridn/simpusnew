@extends('root/root')

@section('content')

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Data Dokter</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Edit Dokter</li>
              </ol>
              <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#myModal" class="model_img img-responsive"><i class="fa fa-plus-circle"></i> Tambah Data</button>
          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Edit Data Dokter</h4>

                  <div class="table-responsive m-t-40">
                              @foreach ($data as $data)
                              <form method="post" action="/simpus/update">
                                @csrf
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">ID Dokter</label>
                                    <input type="text" class="form-control" name="id_dokter" aria-describedby="emailHelp" placeholder="ID Dokter" value="{{ $data->id_dokter }}">
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Dokter</label>
                                    <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" placeholder="Nama Lengkap dan Gelar" value="{{ $data->nama }}">
                                    <small id="emailHelp" class="form-text text-muted">Masukan nama lengkap dan gelar</small>
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputPassword1">Spesialis</label>
                                    <input type="text" class="form-control" name="spesialis" placeholder="Spesialis" value="{{ $data->spesialis }}">
                                  </div>
                                  <input type="text" name="id" value="{{ $data->id }}" hidden>
                                  <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                              </form>
                              @endforeach

                          <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


@endsection
