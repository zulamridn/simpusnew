@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Keuangan Hari ini</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Keuangan</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                            <h3><b>Total Pemasukan</b> <span class="pull-right">Rp {{number_format($total,0,',','.')}}</span></h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Keuangan</h4>

                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>Nama Tindakan</th>
                                                <th>Tarif</th>
                                                <th>Jumlah Tindakan</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>No </th>
                                              <th>Nama Tindakan</th>
                                              <th>Tarif</th>
                                              <th>Jumlah Tindakan</th>
                                              <th>Total</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($hariini as $hariini)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $hariini->tindakan }} </td>
                                                  <td> Rp {{number_format($hariini->tarif,0,',','.')}} </td>
                                                  <td> {{ $hariini->kasus}} </td>
                                                  <td> Rp {{number_format($hariini->biaya,0,',','.')}} </td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
