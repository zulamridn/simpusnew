@extends('root/root')

@section('content')

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Masukan Data Rekam Medis</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Masukan Data Rekam Medis</li>
              </ol>
              <div class="d-flex justify-content-end align-items-center">
                  <a href="/simpus/indexrekamedis" class="btn btn-danger d-none d-lg-block m-l-15" style="color:white" ><i class="fa fa-back"></i> Kembali</a>
              </div>
          </div>
      </div>
  </div>

  <div class="row">
    <div class="col-6">
        <div class="card">
            <div class="card-body">

              <table style="font-size:18px">
                <tr>
                  <td >Nomor RM</td>
                  <td style="padding-left:9px;padding-right:9px">:</td>
                  <td id="rm">{{ $biodata->no_rm }}</td>
                </tr>
                <tr>
                  <td>Nama Lengkap</td>
                  <td style="padding-left:9px;padding-right:9px">:</td>
                  <td id="nama">{{ $biodata->nama }}</td>
                </tr>
                <tr>
                  <td>Umur</td>
                  <td style="padding-left:9px;padding-right:9px">:</td>
                  <td id="umur">{{ $biodata->umur }} tahun</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td style="padding-left:9px;padding-right:9px">:</td>
                  <td id="jk">{{ $biodata->jenis_kelamin }}</td>
                </tr>
                <tr>
                  <td>Kunjungan</td>
                  <td style="padding-left:9px;padding-right:9px">:</td>
                  <td id="kunjungan">{{ $biodata->no_rm }}</td>
                </tr>
              </table>

            </div>
        </div>
        <div class="card">
          <h5 class="card-header">Keluhan</h5>
          <div class="card-body">
            <p class="card-text">{{ $diagnosa->keluhan }}</p>
          </div>
        </div>
        <div class="card">
          <h5 class="card-header">Diagnosa</h5>
          <div class="card-body">
            <p class="card-text">{{ $diagnosa->diagnosa }}</p>
          </div>
        </div>
        <div class="card">
          <h5 class="card-header">Catatan</h5>
          <div class="card-body">
            <p class="card-text">{{ $diagnosa->catatan }}</p>
          </div>
        </div>
      </div>
      <div class="col-6">
          <div class="card">
              <div class="card-body">
                <div class="alert alert-success" role="alert">
                  Pengkodean ICD
                </div>
                <div class="border" style="width:100%; height:150px; border:1px solid grey; padding:20px">
                  <select class="form-control demo" multiple>
                    @foreach ($icd as $icd )
                        <option value="{{ $icd->icd }}">{{ $icd->nama_penyakit }} - {{ $icd->icd}}</option>
                    @endforeach
                  </select>
                </div>
                <a href="#" class="btn btn-info btn-block" style="margin-top:20px" id="simpandata"> Simpan Data  </a>
                  </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <script src="//code.jquery.com/jquery.min.js"></script>
  <script src="/assets/tokenize2.js"></script>
  <script type="text/javascript">
  $('.demo').tokenize2();
  $('#simpandata').click(function(){
    //alert($('.demo').val());
    let icd10 = $('.demo').val();
    let no_register = `{{ $biodata->kode_antrian }}`;
    let no_rm = {{ $biodata->no_rm }}
    $.ajax({
      type: "GET",
      url: "/api/postrm/"+icd10+"/"+no_register+"/"+no_rm,
      //data : { icd10:icd10 },
      success: function(data){
        if(data.status === '1'){
          swal("Data Berhasil Disimpan", "Data Rekam Medis Berhasil di Masukan", "success");
        }else{
          swal("Data Gagal Disimpan", "Data Rekam Medis Gagal di Masukan", "warning");
        }
      }
    })
  })
  </script>


@endsection
