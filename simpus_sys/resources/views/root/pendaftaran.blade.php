@extends('root/root')

@section('content')

<div class="row page-titles">
  <div class="col-md-5 align-self-center">
    <h4 class="text-themecolor">Loket Pendaftaran</h4>
  </div>
  <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
      <a href="/pendaftaran" class="btn btn-success d-none d-lg-block m-l-15" style="color:white"><i class="fa fa-refresh"></i> Refresh</a>
      <a class="btn btn-info d-none d-lg-block m-l-15" style="color:white" data-toggle="modal" data-target="#printersetting"><i class="fa fa-print"></i> Printer Setting</a>
    </div>
  </div>
</div>

<div class="row">

  <div class="col-lg-6 col-md-6">

    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading" id="total">{{ $data->total_antrian }}</h1>
          <hr>
          <p class="mb-0">Total Antrian</p>
        </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-3 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading" id="dipanggil">{{ $data->dipanggil }}</h1>
          <hr>
          <p class="mb-0">Dipanggil</p>
        </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-3 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading">0</h1>
          <hr>
          <p class="mb-0">Antrian Online</p>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading" id="sisa">{{ $data->sisa }}</h1>
          <hr>
          <p class="mb-0">Sisa Antrian</p>
        </div>
      </div>
    </div>
  </div>


  <!-- Column -->
  <!-- Column -->
  <div class="col-lg-6 col-md-6">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-row">
          <div class="row" style="width:100%">
            <div class="col">
              <button style="width:100%;height:100px" class="btn btn-success" id="panggil">Panggil</button>
            </div>
            <div class="col">
              <button style="width:100%;height:100px" class="btn btn-danger" id="ulang">Ulang</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col">
            <div class="alert alert-success" role="alert">
              <h3 class="alert-heading">Data Antrian : <span id="nomorantrian" style="font-size:40px"> </span> / <span id="idantri" style="font-size:30px;color:red"> </span> / <span id="umum" style="font-size:40px"> </span> / <span id="onlinestatus" style="font-size:40px"> </span> </h3>
              <input type="text" id="id_par" hidden />
              <input type="text" id="nik_par" hidden />
              <input type="text" id="kode_antrian_par" hidden />
              <input type="text" id="nomor_antrian" hidden />
              <hr>
              <div class="row">

                <div class="col-md-6">
                  <input type="text" class="form-control" placeholder="Nomor Induk Kependudukan" aria-label="Username" aria-describedby="basic-addon1" id="nik">
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-success btn-block" id="ambildata">Ambil Data</button>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#myModal">Tambah Data</button>
                </div>
                <div class="col-md-2">
                  <button type="button" class="btn btn-success btn-block" id="carirm" data-toggle="modal" data-target="#myRm"> Cari RM</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive m-t-40">
          <div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Tambah Data Pasien</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form method="post" id="create_pasien">
                  @csrf
                  <div class="modal-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nomor Induk Kependudukan</label>
                      <input type="text" class="form-control" name="nik" id="daftar-nik" aria-describedby="emailHelp" placeholder="Nomor Induk Kependudukan" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Lengkap</label>
                      <input type="text" class="form-control" name="nama" id="daftar-nama" aria-describedby="emailHelp" placeholder="Nama Lengkap" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Kelamin</label>
                      <select class="form-control" name="jenis_kelamin" id="daftar-jk">
                        <option value=""> Pilih Jenis Kelamin</option>
                        <option value="L">Laki - Laki</option>
                        <option value="P">Perempuan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tempat Lahir</label>
                      <input type="text" class="form-control" name="tempat_lahir" id="daftar-tempat" aria-describedby="emailHelp" placeholder="Tempat Lahir" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal Lahir</label>
                      <input type="text" class="form-control" class="form-control" name="tanggal_lahir"  placeholder="2017-06-04" id="mdate">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Kepala Keluarga</label>
                      <input type="text" class="form-control" name="nama_kepala_keluarga" id="daftar-kk" aria-describedby="emailHelp" placeholder="Nama Kepala Keluarga" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Status Dalam Keluarga</label>
                      <select class="form-control" name="status_keluarga" id="daftar-status">
                        <option value=""> Pilih Status Keluarga</option>
                        <option value="01">Kepala Keluarga</option>
                        <option value="02">Istri</option>
                        <option value="03">Anak</option>
                        <option value="04">Anggota Keluarga Lain</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nomor Handphone</label>
                      <input type="text" class="form-control" name="no_hp" id="daftar-nohp" aria-describedby="emailHelp" placeholder="Nomor Handphone" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Alamat</label>
                      <textarea class="form-control" name="alamat" id="daftar-alamat" rows="3"></textarea>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button class="btn btn-info waves-effect" id="simpan_pasien">Simpan</button>
              </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>

      <div class="table-responsive m-t-40">
        <div id="myRm" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myRm" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cari dengan Nomor RM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>

              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Masukan No RM</label>
                  <input type="text" class="form-control" name="rm" id="rm" placeholder="Nomor RM" width="100px" required>
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-info waves-effect" id="ambildatarm">Ambil Data</button>
              </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>


      <div class="table-responsive m-t-40">
        <div id="printersetting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="printersetting" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Printer Setting</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div class="modal-body">
                @foreach ($printer as $printer)
                <div class="form-group">
                  <label for="exampleInputEmail1">App Key</label>
                  <input type="text" class="form-control" name="app_key" id="app_key" width="100px" required value="{{ $printer->app_key }}">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Application Port</label>
                  <input type="text" class="form-control" name="app_port" id="app_port" width="100px" required value="{{ $printer->app_port }}">
                </div>
                @endforeach
              </div>
              <div class="modal-footer">
                <button class="btn btn-info waves-effect" id="printsetting">Simpan Setting</button>
              </div>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>

    </div>
  </div>
</div>
</div>

<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col">
        <div class="card" style="height:400px">
          <div class="card-body" style="background-image:url('/assets/img/kartuberobat.png');background-size: cover;">
            <div class="row">
              <div class="col-lg-12">
                <table style="margin-top:80px;font-weight:'bold'; font-size: 16px; margin-left:20px">
                  <tr>
                    <td>NIK</td>
                    <td style="padding-left:10px">:</td>
                    <td id="no_nik"></td>
                  </tr>
                  <tr>
                    <td>No RM</td>
                    <td style="padding-left:10px">:</td>
                    <td id="no_rm"></td>
                  </tr>
                  <tr>
                    <td>Nama</td>
                    <td style="padding-left:10px">:</td>
                    <td id="nama"></td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td style="padding-left:10px">:</td>
                    <td id="ttl"></td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td style="padding-left:10px">:</td>
                    <td id="jk"></td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td style="padding-left:10px">:</td>
                    <td id="alamat"></td>
                  </tr>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="alert alert-success text-center" role="alert">
          <h4 class="alert-heading">Data Antrian Pelayanan - Pilih Poli</h4>
          <hr>
          <center>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              @foreach ($poli as $poli)
              <label class="btn btn-success labelpoli" style="padding:25px" name={{$poli->kode_antrian}} id="">
                <input type="radio" name="poli" value="{{ $poli->kode_antrian }}" id="poli"> {{ $poli->nama_pelayanan }}
              </label>
              @endforeach
            </div>
          </center>
        </div>
        <div style="text-align:center; margin-top:30px">
          <p style="font-size:60px" id='nomor-antrian-pasien'></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <button type="button" name="button" id="simpancetak" class="btn btn-info btn-block">Simpan & Cetak Nomor Antrian</button>
      </div>
      <div class="col-lg-6">
        <button type="button" name="button" id="printdata" class="btn btn-success btn-block">Cetak Data RM</button>
      </div>
    </div>
  </div>
</div>

{{-- <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script> --}}
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="/autocom/jquery.easy-autocomplete.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/recta/dist/recta.js"></script>

<script type="text/javascript">
  $("#nik").easyAutocomplete({
    url: "/api/pasien",
    getValue: "nik",
    list: {
      match: {
        enabled: true
      }
    }

  });

  $("#rm").easyAutocomplete({
    url: "/api/pasien",
    getValue: "no_rm",
    list: {
      match: {
        enabled: true
      }
    }

  });

  $('#ambildata').click(function() {
    var nik = $('#nik').val();
    if (nik == '') {
      swal("Form NIK Kosong", "Pastikan Form NIK Terisi", "error");
    } else {
      $.ajax({
        type: "GET",
        url: "/api/getpasien/" + nik,
        cache: false,
        success: function(data) {

          if (data.status === '1') {
            $('#no_nik').text(data.nik);
            $('#no_rm').text(data.no_rm);
            $('#nama').text(data.nama);
            $('#ttl').text(data.tempat_lahir + ', ' + data.tanggal_lahir);
            $('#jk').text(data.jk);
            $('#alamat').text(data.alamat);
            $('#nik_par').val(nik);
          } else {
            swal("Data Tidak Ditemukan", "", "error");
          }
        }
      })
    }
  });

  $('#ambildatarm').click(function() {
    var rm = $('#rm').val();
    if (rm == '') {
      swal("Form NIK Kosong", "Pastikan Form RM Terisi", "error");
    } else {
      $.ajax({
        type: "GET",
        url: "/api/getpasienrm/" + rm,
        cache: false,
        success: function(data) {

          if (data.status === '1') {
            $('#no_nik').text(data.nik);
            $('#no_rm').text(data.no_rm);
            $('#nama').text(data.nama);
            $('#ttl').text(data.tempat_lahir + ', ' + data.tanggal_lahir);
            $('#jk').text(data.jk);
            $('#alamat').text(data.alamat);
            $('#nik_par').val(nik);
          } else {
            swal("Data Tidak Ditemukan", "", "error");
          }
        }
      })
    }
  });

  $("#simpan_pasien").click(function() {
    
    let daftar_nik = $('#daftar-nik').val();
    let daftar_nama = $('#daftar-nama').val();
    let daftar_jk = $('#daftar-jk').val();
    let daftar_tempat = $('#daftar-tempat').val();
    let daftar_tanggal = $('#mdate').val();
    let daftar_kk = $('#daftar-kk').val();
    let daftar_status = $('#daftar-status').val();
    let daftar_nohp = $('#daftar-nohp').val();
    let daftar_alamat = $('#daftar-alamat').val();

    let data = $('#create_pasien').serialize();
    $.ajax({
      type: "GET",
      url: "/api/create_pasien/"+daftar_nik+"/"+daftar_nama+"/"+daftar_jk+"/"+daftar_tempat+"/"+daftar_tanggal+"/"+daftar_kk+"/"+daftar_status+"/"+daftar_nohp+"/"+daftar_alamat,
      data: data,
      cache: false,
      success: function(data) {
        if (data.status == '1') {
          swal({
              title: "Data Tersimpan",
              text: "Data Pasien Berhasil Tersimpan",
              type: "success",
              icon: "success",
              timer: 2000,
              buttons: false
            })
            .then(() => {
              location.reload();
            })

        } else {
          swal("Belum Ada Data Hari ini", "", "success");
        }
      }
    })
  });

  $('#printdata').click(function() {
      
    let app_key = $('#app_key').val();
    let app_port = $('#app_port').val();

    let no_rm = $('#no_rm').text();
    let nama = $('#nama').text();
    let nik = $('#nik_par').val();

    let printer = new Recta(app_key, app_port);

    printer.open().then(function() {
      printer.align('center')
        .text(nama)
        .bold(true)
        .text(no_rm)
        .bold(false)
        .underline(true)
        .text(nik)
        .cut()
        .print()
    })

  })

  $('#printsetting').click(function() {

    let app_key = $('#app_key').val();
    let app_port = $('#app_port').val();

    $.ajax({
      type: "GET",
      url: "/api/setprint/Pendaftaran/" + app_key + "/" + app_port,
      cache: false,
      success: function(data) {
        if (data.status == '1') {
          swal("Setting Berhasil", "Printer Berhasil di Setting", "success");
        } else {
          swal("Setting gagal", "Silahkan Coba Lagi", "error");
        }
      }
    })

  })

  $('#ulang').click(function() {
    let no_antrian = $('#nomor_antrian').val();
    if (no_antrian == '' || no_antrian == '0') {
      swal("Belum Ada Pasien", "Belum ada pasien mengantri hari ini", "error");
    } else {
      $.ajax({
        type: "GET",
        url: "/api/ulangpanggil/A/" + no_antrian,
        cache: false,
        success: function(data) {
          if (data.status == '1') {
            swal("Panggilan Berhasil", "Tunggu pasien, atau klik tombol ulang jika tidak terdengar", "success");
          } else {
            swal("Belum Ada Pasien", "Belum ada pasien mengantri hari ini", "error");
          }
        }
      })
    }

  })

  $('#panggil').click(function() {
    $.ajax({
      type: "GET",
      url: "/api/panggil",
      cache: false,
      success: function(data) {
        if (data.status === '0') {
          swal("Belum Ada Pasien", "Belum ada pasien mengantri hari ini", "error");
        } else {
          if (data.sisa === '0') {
            swal("Antrian Habis", "Antrian hari ini habis", "error");
          } else {
            $('#dipanggil').text(data.dipanggil);
            $('#sisa').text(data.sisa);
            $('#total').text(data.dipanggil + data.sisa);
            $('#idantri').text(data.data.kode_antrian);
            $('#id_par').val(data.data.kode_antrian);
            $('#nomor_antrian').val(data.data.nomor_antrian);

            $('#no_rm').text('');
            $('#nama').text('');
            $('#ttl').text('');
            $('#jk').text('');
            $('#alamat').text('');
            $('#nik').val('');

            if (data.data.online == '0') {
              $('#onlinestatus').text("Offline");
            } else {
              $('#onlinestatus').text("Online");
            }

            if (data.data.bpjs == '0') {
              $('#umum').text('Umum');
            } else {
              $('#umum').text('BPJS');
            }

            if (data.data.nomor_antrian < 10) {
              $("#nomorantrian").text("A00" + data.data.nomor_antrian);
            } else if (data.data.nomor_antrian >= 10 && data.data.nomor_antrian < 100) {
              $("#nomorantrian").text("A0" + data.data.nomor_antrian);
            } else {
              $("#nomorantrian").text("A0" + data.data.nomor_antrian);
            }

            swal("Panggilan Berhasil", "Tunggu pasien, atau klik tombol ulang jika tidak terdengar", "success");
          }
        }
        console.log(data);

      }
    });
  })

  $('#reset').click(function() {

    swal({
        title: "Apakah Anda Yakin ?",
        text: "Anda Akan Menghapus Seluruh Data Antrian Hari ini",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            type: "GET",
            url: "/api/reset",
            success: function(data) {
              if (data.status == '1') {
                swal({
                    title: "Reset Berhasil",
                    text: "Data Antrian Anda Berhasil di Reset",
                    type: "success",
                    icon: "success",
                    timer: 2000,
                    buttons: false
                  })
                  .then(() => {
                    location.reload();
                  })

              } else {
                swal("Belum Ada Data Hari ini", "", "success");
              }
              // /console.log(data.status);
            }
          })

        } else {
          swal("Anda Membatalkan Reset");
        }
      });



  })

  $(document).ready(function() {
    // Mengambil data antrian terakhir
    $.ajax({
      type: "GET",
      url: "/api/onload",
      cache: false,
      success: function(data) {
        if (data.status === '0') {
          $("#nomorantrian").text('-');
          $('#idantri').text("-");
          $('#onlinestatus').text("-");
          $('#umum').text('-');
        } else {

          $('#idantri').text(data.kode_antrian);
          $('#id_par').val(data.kode_antrian);

          if (data.online == '0') {
            $('#onlinestatus').text("Offline");
          } else {
            $('#onlinestatus').text("Online");
          }

          if (data.bpjs == '0') {
            $('#umum').text('Umum');
          } else {
            $('#umum').text('BPJS');
          }

          if (data.nomor_antrian < 10) {
            $("#nomorantrian").text("A00" + data.nomor_antrian);
          } else if (data.nomor_antrian >= 10 && data.nomor_antrian < 100) {
            $("#nomorantrian").text("A0" + data.nomor_antrian);
          } else {
            $("#nomorantrian").text("A0" + data.nomor_antrian);
          }

        }
        console.log(data);

      }
    });

    $(".labelpoli").click(function() {
      var radioValue = $(this).attr('name');
      $('#kode_antrian_par').val(radioValue);
      console.log(radioValue);
    });

    $("#simpancetak").click(function() {

      var kode_antrian = $('#kode_antrian_par').val();
      var nik = $('#no_nik').text();
      var id_antrian = $('#id_par').val();
      var rm = $('#rm').val();
      let app_key = $('#app_key').val();
      let app_port = $('#app_port').val();
      let rekam_medis = $('#no_rm').text();


      if (kode_antrian === '' || nik === '' || id_antrian === '') {
        swal("Kesalahan", "Lengkapi Data Anda", "error");
      } else {
        console.log("NIK:" + nik);
        console.log("RM ->" + rekam_medis);
        $.ajax({
          type: "GET",
          url: "/api/antrianpoli/" + id_antrian + "/" + rekam_medis + "/" + kode_antrian,
          cache: false,
          success: function(data) {
            if (data.status === '1') {
              console.log(data.antrian)
              $('#nomor-antrian-pasien').text(data.antrian);
              swal("Simpan Data Berhasil", "Data pasien tersimpan di database", "success");
             
              // let printer = new Recta(app_key, app_port);
              // printer.open().then(function() {
              //   printer.align('center')
              //     .bold(true)
  				    //     .text('PUSKESMAS TEBAS')
  				    //     .bold(true)
  				    //     .text('Kab Sambas')
              //     .cut(true,'10')
              //     .bold(false)
              //     .text('Nomor Antrian')
  				    //     .mode('B', true, true, true, true)
  				    //     .text(data.antrian)
  				    //     .cut(true,'10')
  				    //     .mode('B', false, false, false, true)
              //     .text('Kepuasan Anda adalah Prioritas Kami')
  				    //     .mode('B', false, false, false, true)
  				    //     .cut()
  				    //     .print()
              // })
            } else {
              swal("Gagal Menyimpan", "Sudah terdapat pasien pada antrian ini", "error");
            }
          }
        })

      }

    })

  });
</script>

@endsection