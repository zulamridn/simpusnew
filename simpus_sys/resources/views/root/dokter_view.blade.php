@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Dokter</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Dokter</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#myModal" class="model_img img-responsive"><i class="fa fa-plus-circle"></i> Tambah Data</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Dokter</h4>

                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Spesialis</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>No </th>
                                              <th>ID</th>
                                              <th>Nama</th>
                                              <th>Spesialis</th>
                                              <th>#</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($data as $data)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $data->id_dokter }} </td>
                                                  <td> {{ $data->nama }} </td>
                                                  <td> {{ $data->spesialis }} </td>
                                                  <td> <a href='/simpus/edit/{{ $data->id }}' class="btn btn-info" style="color:white"> Edit</a> <a href='/simpus/hapus/{{ $data->id }}' class="btn btn-danger" style="color:white"> Hapus</a></td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Tambah Data Dokter</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form method="post" action="/simpus/tambah_dokter">
                                              @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">ID Dokter</label>
                                                  <input type="text" class="form-control" name="id_dokter" aria-describedby="emailHelp" placeholder="ID Dokter">
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Nama Dokter</label>
                                                  <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" placeholder="Nama Lengkap dan Gelar">
                                                  <small id="emailHelp" class="form-text text-muted">Masukan nama lengkap dan gelar</small>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputPassword1">Spesialis</label>
                                                  <input type="text" class="form-control" name="spesialis" placeholder="Spesialis">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
