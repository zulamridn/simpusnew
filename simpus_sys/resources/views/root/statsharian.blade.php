
@extends('root/root')

@section('content')

  <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Statistik Hari ini</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Statistik Harian</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-success"><i class="ti-wallet"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $total->total_antrian }}</h3>
                                        <h5 class="text-muted m-b-0">Antrian Hari ini</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-info"><i class="ti-user"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $total->dipanggil }}</h3>
                                        <h5 class="text-muted m-b-0">Antrian Dipanggil</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-danger"><i class="ti-calendar"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $total->sisa}}</h3>
                                        <h5 class="text-muted m-b-0">Antrian Belum Dipanggil</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round align-self-center round-success"><i class="ti-settings"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0">{{ $online_today}}</h3>
                                        <h5 class="text-muted m-b-0">Total Antrian Online</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>

                <div class="row">

                  <div class="col-lg-12">
                    <div class="card">

                      <div class="card-body">
                        <center><h4 class="card-title">Antrian Poli</h4></center>
                        <canvas id="bypoli" style="width:100%; height:400px" ><canvas>
                      </div>
                    </div>

                  </div>

                </div>

                <div class="row">
                    <!-- column -->
                    <div class="col-lg-4">

                          <div class="card text-center">
                              <div class="card-body">
                                  <h4 class="card-title">Berdasarkan Asuransi</h4>
                                  <div id="morris-donut-chart-3"></div>
                                  <ul class="list-inline text-center" style="margin-top:15px">
                                      <li>
                                          <h5><i class="fa fa-circle m-r-5 text-info"></i>BPJS {{ $bpjs }} Orang</h5>
                                      </li>
                                      <li>
                                          <h5><i class="fa fa-circle m-r-5 text-success"></i>Umum {{ $umum }} Orang</h5>
                                      </li>
                                  </ul>
                              </div>
                          </div>

                    </div>
                    <div class="col-lg-4">

                          <div class="card text-center">
                              <div class="card-body">
                                  <h4 class="card-title">Berdasarkan Jenis Kelamin</h4>
                                    <div id="morris-donut-chart-2"></div>
                                    <ul class="list-inline text-center" style="margin-top:15px">
                                        <li>
                                            <h5><i class="fa fa-circle m-r-5 text-info"></i>Laki laki {{ $laki }} Orang</h5>
                                        </li>
                                        <li>
                                            <h5><i class="fa fa-circle m-r-5 text-success"></i>Perempuan {{ $perempuan }} Orang</h5>
                                        </li>
                                    </ul>
                              </div>
                          </div>

                    </div>

                    <div class="col-lg-4">

                          <div class="card text-center">
                              <div class="card-body">
                                  <h4 class="card-title">Berdasarkan Pendaftaran</h4>
                                    <div id="morris-donut-chart-4"></div>
                                    <ul class="list-inline text-center" style="margin-top:15px">
                                        <li>
                                            <h5><i class="fa fa-circle m-r-5 text-info"></i>Online {{ $online }} Orang</h5>
                                        </li>
                                        <li>
                                            <h5><i class="fa fa-circle m-r-5 text-success"></i>Offline {{ $offline }} Orang</h5>
                                        </li>
                                    </ul>
                              </div>
                          </div>

                    </div>


                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-12">
                      <div class="card">
                            <div class="card-body">
                                <h5 class="card-title m-b-0">Daftar List Penyakit</h5>
                                <p class="text-muted">10 Penyakit Terbanyak Hari ini</p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Penyakit</th>
                                                <th>Kasus</th>
                                                <th>Kode</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php $no = 1; ?>
                                          @foreach ($penyakit as $penyakit)

                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $penyakit->nama_penyakit }}</td>
                                                <td>{{ $penyakit->total}}</td>
                                                <td><span class="label label-danger">{{ $penyakit->kode_rm }} </span> </td>
                                            </tr>
                                          @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->

                    <!-- column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->

                <script src="/simpus/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
                <script src="/simpus/assets/node_modules/raphael/raphael-min.js"></script>
                <script src="/simpus/assets/node_modules/morrisjs/morris.min.js"></script>
                <script src="/simpus/assets/node_modules/Chart.js/chartjs.init.js"></script>
                <script src="/simpus/assets/node_modules/Chart.js/Chart.min.js"></script>
                <script type="text/javascript">
                var ctx1 = document.getElementById("bypoli").getContext("2d");
                    Morris.Donut({
                        element: 'morris-donut-chart-2',
                        data: [{
                            label: "Laki Laki",
                            value: {{ $laki }},

                        }, {
                            label: "Perempuan",
                            value: {{ $perempuan }}
                        }],
                        resize: true,
                        colors:['#009efb', '#55ce63', '#2f3d4a']
                    });

                    Morris.Donut({
                        element: 'morris-donut-chart-3',
                        data: [{
                            label: "BPJS",
                            value: {{ $bpjs }},

                        }, {
                            label: "Umum",
                            value: {{ $umum }}
                        }],
                        resize: true,
                        colors:['#009efb', '#55ce63']
                    });

                    Morris.Donut({
                        element: 'morris-donut-chart-4',
                        data: [{
                            label: "Online",
                            value: {{ $online }},

                        }, {
                            label: "Offline",
                            value: {{ $offline }}
                        }],
                        resize: true,
                        colors:['#009efb', '#55ce63']
                    });

                    $.ajax({
                      type: "GET",
                      url: "/simpus/api/statistik_bypoli",
                      cache: false,
                      success: function(data){
                        console.log(data)
                        let nama = [];
                        let total = [];
                        for(var i = 0; i < data.length; i++) {
                          nama.push(data[i].nama_pelayanan)
                          total.push(data[i].poli)

                        }
                        var data1 = {
                            labels: nama,
                            datasets: [
                                {
                                    label: "My First dataset",
                                    fillColor: "rgba(152,235,239,0.8)",
                                    strokeColor: "rgba(152,235,239,0.8)",
                                    pointColor: "rgba(152,235,239,1)",
                                    pointStrokeColor: "#fff",
                                    pointHighlightFill: "#fff",
                                    pointHighlightStroke: "rgba(152,235,239,1)",
                                    data: total
                                }

                            ]
                        };

                        var chart1 = new Chart(ctx1).Line(data1, {
                            scaleShowGridLines : true,
                            scaleGridLineColor : "rgba(0,0,0,.005)",
                            scaleGridLineWidth : 0,
                            scaleShowHorizontalLines: true,
                            scaleShowVerticalLines: true,
                            bezierCurve : true,
                            bezierCurveTension : 0.4,
                            pointDot : true,
                            pointDotRadius : 4,
                            pointDotStrokeWidth : 1,
                            pointHitDetectionRadius : 2,
                            datasetStroke : true,
                        tooltipCornerRadius: 2,
                            datasetStrokeWidth : 2,
                            datasetFill : true,
                            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                            responsive: true
                        });
                      }
                    })

                </script>


@endsection
