
@extends('root/root')

@section('content')

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Data Tarif</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Edit Tarif</li>
              </ol>

          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Edit Data Tarif</h4>

                  <div class="table-responsive m-t-40">
                              @foreach ($data as $data)
                              <form method="post" action="/simpus/update_tarif">
                                @csrf
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Tindakan</label>
                                    <input type="text" class="form-control" name="nama_tindakan" placeholder="ID Dokter" value="{{ $data->nama_tindakan}}">
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Tarif</label>
                                    <input type="text" class="form-control" name="tarif" aria-describedby="emailHelp" placeholder="Nama Lengkap dan Gelar" value="{{ $data->tarif}}">
                                  </div>

                                  <input type="text" name="id" value="{{ $data->id }}" hidden>
                                  <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                              </form>
                              @endforeach

                          <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


@endsection
