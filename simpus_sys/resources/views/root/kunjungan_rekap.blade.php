@extends('root/root')

@section('content')
  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Rekap Kunjungan</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Data Rekap Kunjungan</li>
              </ol>
          </div>
      </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
            <div class="card-body">
                <h5 class="card-title m-b-0">Daftar Kunjungan {{ $filter }} {{ $hari }} </h5>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:30px">#</th>
                                <th>Nama Poli</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no = 1; ?>
                          @foreach ($kunjungan as $kunjungan)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $kunjungan->nama_pelayanan }}</td>
                                <td>{{ $kunjungan->poli}} Pasien</td>

                            </tr>
                          @endforeach


                        </tbody>
                    </table>
                </div>

                <div class="row">
                  <div class="col-lg-4">
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>BPJS</th>
                                    <th>Umum</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $bpjs }} Pasien</td>
                                    <td>{{ $umum }} Pasien</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Online</th>
                                    <th>Offline</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $online }} Pasien</td>
                                    <td>{{ $offline }} Pasien</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>

                  <div class="col-lg-4">
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Laki Laki</th>
                                    <th>Perempuan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $pria }} Pasien</td>
                                    <td>{{ $wanita }} Pasien</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>

                </div>

            </div>
        </div>
    </div>
  </div>


@endsection
