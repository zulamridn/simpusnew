@extends('root/root')

@section('content')
  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Rekap Tanggal</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Data Penyakit Terbanyak</li>
              </ol>
          </div>
      </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
            <div class="card-body">
                <h5 class="card-title m-b-0">Daftar List Penyakit</h5>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:30px">#</th>
                                <th>Tanggal</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no = 1; ?>
                          @foreach ($hari as $hari)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ date('l, d F Y', strtotime($hari->date))}}  </td>
                                <td><center> <a role="button" href="rekap_harian_rm/{{$hari->date}}" class="btn btn-info" style="color:white"><i class="fa fa-eye"></i>&nbsp&nbsp&nbspLihat Data</a> </center></td>
                            </tr>
                          @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>


@endsection
