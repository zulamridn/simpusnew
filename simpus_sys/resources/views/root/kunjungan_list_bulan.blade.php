@extends('root/root')

@section('content')
  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Rekap Bulan</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Data Kunjungan</li>
              </ol>
          </div>
      </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
            <div class="card-body">
                <h5 class="card-title m-b-0">Daftar Kunjungan</h5>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:30px">#</th>
                                <th>Bulan / Tahun</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no = 1; ?>
                          @foreach ($bulan as $bulan)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>Bulan - {{date('F' ,strtotime($bulan->month))}}, {{$bulan->year}}  </td>
                                <td><center> <a role="button" href="/simpus/rekap_bulanan_kunj/{{$bulan->month}}/{{$bulan->year}}" class="btn btn-info" style="color:white"><i class="fa fa-eye"></i>&nbsp&nbsp&nbspLihat Data</a> </center></td>
                            </tr>
                          @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>


@endsection
