@extends('root/root')

@section('content')

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Pasien</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Edit Pasien</li>
              </ol>
          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                
                  <div class="table-responsive m-t-40">
                              @foreach ($data as $data)
                                <form method="post"  action="/simpus/update_pasien">
                                  @csrf
                                <div class="modal-body">
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nomor Rekam Medis</label>
                                    <input type="text" class="form-control" name="no_rm" value="{{ $data->no_rm }}" hidden>
                                  </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nomor Induk Kependudukan</label>
                                      <input type="text" class="form-control" name="nik" aria-describedby="emailHelp" placeholder="Nomor Induk Kependudukan" value="{{ $data->nik }}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nama Lengkap</label>
                                      <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{ $data->nama }}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Jenis Kelamin</label>
                                      <select class="form-control" name="jenis_kelamin">
                                        <option value=""> Pilih Jenis Kelamin</option>
                                        <option value="L" {{ ($data->jenis_kelamin === 'L') ? 'selected' : '' }}>Laki - Laki</option>
                                        <option value="P" {{ ($data->jenis_kelamin === 'P') ? 'selected' : '' }}>Perempuan</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Tempat Lahir</label>
                                      <input type="text" class="form-control" name="tempat_lahir" aria-describedby="emailHelp" placeholder="Tempat Lahir" value="{{ $data->tempat_lahir }}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Tanggal Lahir</label>
                                      <input type="text" class="form-control" class="form-control" name="tanggal_lahir" placeholder="2017-06-04" id="mdate" value="{{ $data->tanggal_lahir }}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nama Kepala Keluarga</label>
                                      <input type="text" class="form-control" name="nama_kepala_keluarga" aria-describedby="emailHelp" placeholder="Nama Kepala Keluatga" value="{{ $data->nama_kepala_keluarga }}" disabled>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Status Dalam Keluarga</label>

                                      <select class="form-control" name="status_keluarga" disabled>
                                        <option value=""> Pilih Status Keluarga</option>
                                        <option value="01" {{ ($data->status === 'Kepala Keluarga') ? 'selected' : '' }}>Kepala Keluarga</option>
                                        <option value="02" {{ ($data->status === 'Istri') ? 'selected' : '' }}>Istri</option>
                                        <option value="03" {{ ($data->status === 'Anak') ? 'selected' : '' }}>Anak</option>
                                        <option value="04" {{ ($data->status === 'Anggota Keluara Lainnya') ? 'selected' : '' }}>Anggota Keluarga Lain</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Nomor Handphone</label>
                                      <input type="text" class="form-control" name="no_hp" aria-describedby="emailHelp" placeholder="Nomor Handphone" value="{{ $data->no_hp }}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleFormControlTextarea1">Alamat</label>
                                      <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" rows="3"> {{$data->alamat}} </textarea>
                                    </div>
                                    <div class="form-group">
                                      <button type="submit" class="btn btn-info">Update Data Pasien</button>
                                    </div>

                                  </form>
                              @endforeach

                          <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


@endsection
