@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Setting</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Setting</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#myModal" class="model_img img-responsive"><i class="fa fa-plus-circle"></i> Edit Setting</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">


                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>Logo</th>
                                                <th>Background</th>
                                                <th>Nomor Ambulance</th>
                                                <th>Batas Online</th>
                                                <th>Jam Awal</th>
                                                <th>Jam Akhir</th>
                                                <th>Vidio</th>
                                                <th>Running Text</th>
                                                 <th>Volume NonCall</th>
                                              <th>Voulume OnCall</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                          <tr>
                                              <th>No </th>
                                              <th>Nama</th>
                                              <th>Alamat</th>
                                              <th>Logo</th>
                                              <th>Background</th>
                                              <th>Nomor Ambulance</th>
                                              <th>Batas Online</th>
                                              <th>Jam Awal</th>
                                              <th>Jam Akhir</th>
                                              <th>Vidio</th>
                                            <th>Running Text</th>
                                            <th>Volume NonCall</th>
                                              <th>Voulume OnCall</th>
                                          </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($data as $data)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $data->nama }} </td>
                                                  <td> {{ $data->alamat }} </td>
                                                  <td> <center><img src="{{ $data->logo }}" width="100" alt=""></center></td>
                                                  <td> <center><img src="{{ $data->back2 }}" width="300" alt=""> </center></td>
                                                  <td> {{ $data->no_ambulance }} </td>
                                                  <td> {{ $data->batas_online }} </td>
                                                  <td> {{ $data->jam_awal }} </td>
                                                  <td> {{ $data->jam_akhir }} </td>
                                                  <td> {{ $data->vidio }} </td>
                                                  <td> {{ $data->text }} </td>
                                                  <td> {{ $data->volume_none }} </td>
                                                  <td> {{ $data->volume_oncall }} </td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Edit Setting</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form method="POST" action="/simpus/edit_setting" enctype="multipart/form-data">
                                              @csrf
                                            <div class="modal-body">

                                                  <div class="form-group">
                                                    <label for="exampleInputEmail1">Nama Puskesmas</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="Nama Puskesmas" value="{{ $data['nama'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputEmail1">Alamat Puskesmas</label>
                                                    <input type="text" class="form-control" name="alamat" placeholder="Nama Puskesmas" value="{{ $data['alamat'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputEmail1">Logo</label>
                                                    <input type="text" class="form-control" name="logo" value="{{ $data['logo'] }}">
                                                    <small id="emailHelp" class="form-text text-muted">Silahkan Copy dan Paste Link Logo Anda</small>
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputEmail1">Background</label>
                                                    <input type="text" class="form-control" name="back2"  value="{{ $data['back2'] }}">
                                                    <small id="emailHelp" class="form-text text-muted">Silahkan Copy dan Paste Link Background Anda </small>
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputPassword1">Nomor Ambulance</label>
                                                    <input type="text" class="form-control" name="no_ambulance" value="{{ $data['no_ambulance'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputPassword1">Batas Online Harian</label>
                                                    <input type="text" class="form-control" name="batas_online" value="{{ $data['batas_online'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputPassword1">Jam Awal Antrian (ex. 8)</label>
                                                    <input type="text" class="form-control" name="jam_awal" value="{{ $data['jam_awal'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputPassword1">Jam Akhir Antrian (ex. 11)</label>
                                                    <input type="text" class="form-control" name="jam_akhir" value="{{ $data['jam_akhir'] }}">
                                                  </div>
                                                   <div class="form-group">
                                                    <label for="exampleInputPassword1">Volume Tanpa Panggilan</label>
                                                    <input type="text" class="form-control" name="volume_none" value="{{ $data['volume_none'] }}">
                                                  </div>
                                                   <div class="form-group">
                                                    <label for="exampleInputPassword1">Vulume Saat Panggilang</label>
                                                    <input type="text" class="form-control" name="volume_oncall" value="{{ $data['volume_oncall'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputPassword1">Vidio</label>
                                                    <input type="text" class="form-control" name="vidio" value="{{ $data['vidio'] }}">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="exampleInputPassword1">Running Text</label>
                                                    <input type="text" class="form-control" name="text" value="{{ $data['text'] }}">
                                                  </div>
                                                  <input type="text" name="id" value="{{ $data['id'] }}" hidden>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-lg-6">

                  </div>
                  <div class="col-lg-6">

                  </div>
                </div>
@endsection
