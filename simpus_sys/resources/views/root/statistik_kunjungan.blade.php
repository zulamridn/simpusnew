@extends('root/root')

@section('content')



  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Statistik Kunjungan</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Statistik Kunjungan</li>
              </ol>

          </div>
      </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Data Keuangan Bulan ini</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphPerTanggal" style="width:100%; height:400px" ><canvas>
            </div>
          </div>

          <div class="card">
            <div class="card-body">
              <div class="table-responsive m-t-40">
                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th style="width:30px">#</th>
                              <th>Tanggal</th>
                              <th>#</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach ($rekaptanggal as $rekaptanggal)
                          <tr>
                              <td>{{ $no++ }}</td>
                              <td>{{date('D, d M Y', strtotime($rekaptanggal->tanggal))}}  </td>
                              <td>{{$rekaptanggal->total}}  </td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Data Keuangan Tahun ini</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphPerBulan" style="width:100%; height:400px" ><canvas>
            </div>
          </div>

          <div class="card">
            <div class="card-body">
              <div class="table-responsive m-t-40">
                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th style="width:30px">#</th>
                              <th>Bulan</th>
                              <th>#</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach ($rekapbulan as $rekapbulan)
                          <tr>
                              <td>{{ $no++ }}</td>
                              <td>{{date('F', strtotime($rekapbulan->bulan))}}  </td>
                              <td>{{$rekapbulan->total}}  </td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Data Keuangan Sepanjang Tahun</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphPerTahun" style="width:100%; height:400px" ><canvas>
            </div>
          </div>


      <div class="card">
        <div class="card-body">
          <div class="table-responsive m-t-40">
              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th style="width:30px">#</th>
                          <th>Tahun</th>
                          <th>#</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach ($rekaptahun as $rekaptahun)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{date($rekaptahun->tahun)}}  </td>
                          <td>{{$rekaptahun->total}}  </td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>



    </div>
  </div>
    <script src="/simpus/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
  <script src="/simpus/assets/node_modules/Chart.js/chartjs.init.js"></script>
  <script src="/simpus/assets/node_modules/Chart.js/Chart.min.js"></script>
  <script>
    var ctx1 = document.getElementById("graphPerTanggal").getContext("2d");
    var ctx2 = document.getElementById("graphPerBulan").getContext("2d");
    var ctx3 = document.getElementById("graphPerTahun").getContext("2d");

    $('document').ready(function(){

      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_kunj_json",
        cache: false,
        success: function(data){
          //console.log(data)
          let tanggal = [];
          let total = [];
          for(var i = 0; i < data.length; i++) {
            total.push(data[i].total)
            tanggal.push(data[i].tanggal)
            console.log('tanggal:'+tanggal);
          }
          var data1 = {
              labels: tanggal,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: total
                  }

              ]
          };

          var chart1 = new Chart(ctx1).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_kunj_json_bul",
        cache: false,
        success: function(data){
          //console.log(data)
          let bulan = [];
          let totalperbulan = [];
          for(var i = 0; i < data.length; i++) {
            totalperbulan.push(data[i].total)
            bulan.push(data[i].bulan)
            //console.log('tanggal:'+tanggal);
          }
          var data1 = {
              labels: bulan,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: totalperbulan
                  }

              ]
          };

          var chart2 = new Chart(ctx2).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_kunj_json_tah",
        cache: false,
        success: function(data){
          //console.log(data)
          let tahun= [];
          let totalpertahun = [];
          for(var i = 0; i < data.length; i++) {
            totalpertahun.push(data[i].total)
            tahun.push(data[i].tahun)
            //console.log('tanggal:'+tanggal);
          }
          var data1 = {
              labels: tahun,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: totalpertahun
                  }

              ]
          };

          var chart3 = new Chart(ctx3).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


    });
  </script>

@endsection
