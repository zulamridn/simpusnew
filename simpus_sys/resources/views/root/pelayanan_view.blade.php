@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Loket Pelayanan</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Loket Pelayanan</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#myModal" class="model_img img-responsive"><i class="fa fa-plus-circle"></i> Tambah Data</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Loket Pelayanan</h4>

                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>Nama Pelayanan</th>
                                                <th>Nama Dokter</th>
                                                <th>Kode Antrian</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>No </th>
                                              <th>Nama Pelayanan</th>
                                              <th>Nama Dokter</th>
                                              <th>Kode Antrian</th>
                                              <th>#</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($data as $data)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $data->nama_pelayanan }} </td>
                                                  <td> {{ $data->nama }} </td>
                                                  <td> {{ $data->kode_antrian }} </td>
                                                  <td> <center> <a href='/simpus/hapus_pelayanan/{{ $data->id }}/{{$data->kode_antrian}}' class="btn btn-danger" style="color:white"> Hapus</a> </center></td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Tambah Data Dokter</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form method="post" action="/simpus/tambah_pelayanan">
                                              @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Nama Pelayanan / Ruang / Loket</label>
                                                  <input type="text" class="form-control" name="nama_pelayanan" aria-describedby="emailHelp" placeholder="Nama Pelayanan" required>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Nama Dokter</label>
                                                  {{-- <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" placeholder="Nama Lengkap dan Gelar">
                                                  <small id="emailHelp" class="form-text text-muted">Masukan nama lengkap dan gelar</small> --}}
                                                  <select class="form-control" name="id_dokter">
                                                    <option value=""> Pilih Dokter</option>
                                                    @foreach ($dokter as $dokter)
                                                      <option value="{{ $dokter->id_dokter }}">{{ $dokter->nama }}</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputPassword1">Kode Antrian-</label>
                                                  <select class="form-control" name="kode_antrian">
                                                    <option value=""> Pilih Kode Antrian</option>
                                                    @foreach ($kode as $kode)
                                                      <option value="{{ $kode->kode }}">{{ $kode->kode }}</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Username</label>
                                                  <input type="text" class="form-control" name="username" aria-describedby="emailHelp" placeholder="Username" required>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Password</label>
                                                  <input type="text" class="form-control" name="password" aria-describedby="emailHelp" placeholder="Password" required>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
