
@extends('root/root')

@section('content')

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Data ICD</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Edit Data ICD</li>
              </ol>

          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Edit Data ICD</h4>

                  <div class="table-responsive m-t-40">
                              @foreach ($data as $data)
                              <form method="post" action="/simpus/update_icd">
                                @csrf
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Penyakit</label>
                                    <input type="text" class="form-control" name="nama_penyakit" placeholder="ID Dokter" value="{{ $data->nama_penyakit}}">
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Kode ICD X</label>
                                    <input type="text" class="form-control" name="icd" value="{{ $data->icd}}" disabled>
                                  </div>

                                  <input type="text" name="id" value="{{ $data->id }}" hidden>
                                  <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                              </form>
                              @endforeach

                          <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


@endsection
