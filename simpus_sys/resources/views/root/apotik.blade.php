@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Apotik</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Data Pasien</li>
                            </ol>
                            <div class="d-flex justify-content-end align-items-center">
                                <a href="/simpus/indexapotik" class="btn btn-success d-none d-lg-block m-l-15" style="color:white" ><i class="fa fa-refresh"></i> Refresh</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Pasien Hari ini</h4>

                                <div class="table-responsive m-t-40">
                                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>No </th>
                                              <th>Antrian</th>
                                              <th>No Registrasi</th>
                                              <th>No RM</th>
                                              <th>Nama Pasien</th>
                                              <th>Umur</th>
                                              <th>Jenis Kelamin</th>
                                              <th>#</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                            <th>No </th>
                                            <th>Antrian</th>
                                            <th>No Registrasi</th>
                                            <th>No RM</th>
                                            <th>Nama Pasien</th>
                                            <th>Umur</th>
                                            <th>Jenis Kelamin</th>
                                            <th>#</th>
                                          </tr>
                                      </tfoot>
                                      <tbody>

                                            <?php $no = 1; ?>
                                            @foreach ($data as $data)
                                              <tr>
                                                <td> {{ $no++ }}   </td>
                                                <td> {{ $data->nomor_antrian }} </td>
                                                <td> {{ $data->kode_antrian }} </td>
                                                <td> {{ $data->no_rm}} </td>
                                                <td> {{ $data->nama }} </td>
                                                <td> {{ $data->umur }} Tahun</td>
                                                <td> {{ $data->jenis_kelamin }} </td>
                                                <td> <center><a href='/simpus/apotikinput/{{ $data->id }}/{{ $data->kode_antrian }}' class="btn btn-info" style="color:white"> Masukan Data Obat</a></center></td>
                                              </tr>
                                            @endforeach

                                      </tbody>
                                  </table>
                                    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Tambah Data Dokter</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <form method="post" action="/simpus/tambah_dokter">
                                              @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">ID Dokter</label>
                                                  <input type="text" class="form-control" name="id_dokter" aria-describedby="emailHelp" placeholder="ID Dokter">
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Nama Dokter</label>
                                                  <input type="text" class="form-control" name="nama" aria-describedby="emailHelp" placeholder="Nama Lengkap dan Gelar">
                                                  <small id="emailHelp" class="form-text text-muted">Masukan nama lengkap dan gelar</small>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputPassword1">Spesialis</label>
                                                  <input type="text" class="form-control" name="spesialis" placeholder="Spesialis">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
