@extends('root/root')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Invoice</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Invoice</li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
            <h3><b>Jenis Pembayaran</b> <span class="pull-right">@if($pasien->bpjs == '1') BPJS @else UMUM @endif </span></h3>
              <hr>
              <h4 class="font-bold">{{$pasien->nama}}</h4>
              <p class="text-muted m-l-30">{{ $pasien->no_rm }} / No RM</p>
              <p class="text-muted m-l-30">{{ $pasien->no_hp}} / No HP</p>
              <p class="text-muted m-l-30">{{ $pasien->alamat }} / Alamat</p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    <div class="card">
            <div class="card-body">
            <h3><b>Biaya Lainnya</b></h3>
            <hr>
            <div class="border" style="width:100%; height:150px; border:1px solid grey; padding:20px">
                <select class="form-control demo" multiple>
                    @foreach ($tarif as $tarif )
                        <option value="{{ $tarif->id}}">{{ $tarif->nama_tindakan}} - {{ number_format($tarif->tarif,0,',','.') }}</option>
                    @endforeach
                </select>
            </div>
            <input type="text" id="kode_antrian" value="{{ $pasien->kode_antrian }}" hidden>

            <button class="btn btn-info tambah_biaya btn-block" style="margin-top:15px"> Tambah Biaya </button>
            </div>
        </div>
    </div>
</div>

<div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h3><b>INVOICE</b> <span class="pull-right">#{{$pasien->kode_antrian}}</span></h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <address>
                                                <h3> &nbsp;<b class="text-danger">{{$setting->nama}}</b></h3>
                                                <p class="text-muted m-l-5">{{ $setting->alamat }}</p>
                                            </address>
                                        </div>
                                        <div class="pull-right text-right">
                                            <address>
                                                <h3>Kepada,</h3>
                                                <h4 class="font-bold">{{$pasien->nama}}</h4>
                                                <p class="text-muted m-l-30">{{ $pasien->alamat }}</p>
                                                <p class="text-muted m-l-5">{{ $pasien->no_hp }}</p>
                                                <p class="m-t-30"><b>Tanggal :</b> {{ date(' d M Y')  }}</p>
                                            </address>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive m-t-40">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Nama Tindakan</th>
                                                        <th class="text-right">Biaya</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php  $no = 1 ?>
                                                @foreach ($biaya as $biaya)
                                                    <tr>
                                                        <td class="text-center">{{ $no++ }}</td>
                                                        <td><button class="btn btn-xs btn-danger" onclick="hapus( {{$biaya->id}} )"><b>X</b></button> - {{$biaya->nama_tindakan}}</td>
                                                        <td class="text-right">{{ number_format($biaya->tarif,0,',','.') }}</td>
                                                    </tr>
                                                    <tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pull-right m-t-30 text-right">
                                            <h3><b>Total :</b> {{ number_format($biaya_total,0,',','.') }}</h3> </div>
                                            <input type="text" id="total_biaya" value="{{ $biaya_total }}" hidden>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="text-right save-control">
                                            <button class="btn btn-success" ><i class="fa fa-print"></i>&nbsp&nbsp&nbspPrint dan Simpan </button>
                                            <button class="btn btn-info" id="simpan_data"><i class="fa fa-save"></i>&nbsp&nbsp&nbspSimpan </button>
                                        </div>
                                        <div class="text-right back-control">
                                            <button class="btn btn-danger" onclick="back()"><i class="fa fa-refresh"></i>&nbsp&nbsp&nbspKembali </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
                <script src="/autocom/jquery.easy-autocomplete.min.js"></script>
                <script src="/assets/tokenize2.js"></script>
                <script>
                    $('.demo').tokenize2();
                    $('.back-control').hide();
                </script>
                <script>
                    $('.tambah_biaya').click(function(){
                        let tindakan = $('.demo').val();
                        let reg = $('#kode_antrian').val();
                        $.ajax({
                        type: "GET",
                        url: "/api/tambah_biaya/"+reg+"/"+tindakan,
                        cache: false,
                        success: function(data){
                          if(data.status === '1'){
                            location.reload();
                          }
                        }
                      });
                    })

                    function hapus(id){
                        $.ajax({
                            type: "GET",
                            url: "/api/hapus_biaya/"+id,
                            cache: false,
                            success: function(data){

                            if(data.status === '1'){
                                location.reload();
                            }

                            }
                        });
                    }

                    function back(){
                        window.location.href = '/kasir';
                    }

                    $('#simpan_data').click(function(){
                        let reg = $('#kode_antrian').val();
                        let total = $('#total_biaya').val();

                        $.ajax({
                            type: "GET",
                            url: "/api/simpan_biaya/"+reg+"/"+total,
                            cache: false,
                            success: function(data){
                                if(data.status === '1'){
                                    $('.back-control').show();
                                    $('.save-control').hide();
                                }
                            }
                        });

                    })
                </script>

@endsection
