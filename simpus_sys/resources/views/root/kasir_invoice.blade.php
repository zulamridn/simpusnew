@extends('root/root')
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Invoice</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Invoice</li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
                    <div class="col-md-12">
                        <div class="card" id="invoice">
                            <div class="card-body">
                                <h3><b>INVOICE</b> <span class="pull-right">#{{$pasien->kode_antrian}}</span></h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <address>
                                                <h3> &nbsp;<b class="text-danger">{{$setting->nama}}</b></h3>
                                                <p class="text-muted m-l-5">{{ $setting->alamat }}</p>
                                            </address>
                                        </div>
                                        <div class="pull-right text-right">
                                            <address>
                                                <h3>Kepada,</h3>
                                                <h4 class="font-bold">{{$pasien->nama}}</h4>
                                                <p class="text-muted m-l-30">{{ $pasien->alamat }}</p>
                                                <p class="text-muted m-l-5">{{ $pasien->no_hp }}</p>
                                                <p class="m-t-30"><b>Tanggal :</b> {{ date(' d M Y')  }}</p>
                                            </address>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive m-t-40">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th>Nama Tindakan</th>
                                                        <th class="text-right">Biaya</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php  $no = 1 ?>
                                                @foreach ($biaya as $biaya)
                                                    <tr>
                                                        <td class="text-center">{{ $no++ }}</td>
                                                        <td>{{$biaya->nama_tindakan}}</td>
                                                        <td class="text-right">{{ number_format($biaya->tarif,0,',','.') }}</td>
                                                    </tr>
                                                    <tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pull-right m-t-30 text-right">
                                            <h3><b>Total :</b> {{ number_format($biaya_total,0,',','.') }}</h3> </div>
                                            <input type="text" id="total_biaya" value="{{ $biaya_total }}" hidden>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="text-right save-control">
                                            <button class="btn btn-success" onclick="print()"><i class="fa fa-print"></i>&nbsp&nbsp&nbspPrint </button>
                                            <button class="btn btn-danger" onclick="back()"><i class="fa fa-refresh"></i>&nbsp&nbsp&nbspKembali </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>

                <script>


                    function back(){
                        window.location.href = '/indexkeuangan';
                    }

                    function print() {
                         var printContents = document.getElementById('invoice').innerHTML;
                         var originalContents = document.body.innerHTML;

                         document.body.innerHTML = printContents;

                         window.print();

                         document.body.innerHTML = originalContents;
                    }
                </script>

@endsection
