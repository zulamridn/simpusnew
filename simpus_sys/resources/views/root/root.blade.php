<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x14" href="http://puskesmastebas.sambas.go.id/wp-content/uploads/2020/01/abu-abu.png">
    <title>Simpus Terintegrasi</title>
    <link href="/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <link href="/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="/dist/css/style.min.css" rel="stylesheet">
    <link href="/dist/css/pages/dashboard1.css" rel="stylesheet">

    <link rel="stylesheet" href="/autocom/easy-autocomplete.min.css">
    <link rel="stylesheet" href="/autocom/easy-autocomplete.themes.min.css">
    <link href="/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="/assets/node_modules/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/tokenize2.css">
    <link rel="stylesheet" href="/assets/node_modules/html5-editor/bootstrap-wysihtml5.css" />
</head>

<body class="skin-megna fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Puskesmas Tebas</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="http://puskesmastebas.sambas.go.id/wp-content/uploads/2020/01/putih.png" alt="homepage" class="dark-logo" width="35" />
                            <!-- Light Logo icon -->
                            <img src="http://puskesmastebas.sambas.go.id/wp-content/uploads/2020/01/putih.png" alt="homepage" class="light-logo" width="35" />
                        </b>
                        <!--End Logo icon -->
                        <span class="hidden-xs"><span class="font-bold">pkm</span>tebas</span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->

                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">

                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="hidden-md-down">{{Session::get('username')}}<i class="fa fa-angle-down"></i></span> </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">

                                <!-- text-->
                                <a href="/logout" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                                <!-- text-->
                            </div>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">

                        @if (Session::get('akses') == 'admin')
                        <li> <a class="waves-effect waves-dark" href="/statistik"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu">Master Data</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/dokter">Master Dokter</a></li>
                                <li><a href="/pelayanan">Master Loket / Pelayanan</a></li>
                                <li><a href="/tarif">Master Tarif</a></li>
                            </ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="/download"><i class="fa fa-download"></i><span class="hide-menu">Download</span></a>
                        <li> <a class="waves-effect waves-dark" href="/setting"><i class="icon-settings"></i><span class="hide-menu">Settings</span></a>
                            @endif


                            @if (Session::get('akses') == 'pendaftaran')
                        <li> <a class="waves-effect waves-dark" href="/pendaftaran"><i class="icon-speedometer"></i><span class="hide-menu">Panggil</span></a>
                        <li> <a class="waves-effect waves-dark" href="/pasien_list"><i class="icon-user"></i><span class="hide-menu">Pasien</span></a>
                            @endif

                            @if (Session::get('akses') == 'poli')
                        <li> <a class="waves-effect waves-dark" href="/indexpoli"><i class="icon-speedometer"></i><span class="hide-menu">Panggil</span></a>
                        <li> <a class="waves-effect waves-dark" href="/pasien_list"><i class="icon-user"></i><span class="hide-menu">Pasien</span></a>
                            @endif

                            @if (Session::get('akses') == 'statistik')
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Statistik</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/statistik">Hari ini</a></li>
                                <li><a href="/statistik_kunj">Kunjungan</a></li>
                                <li><a href="/statistik_keu">Keuangan</a></li>
                                <li><a href="/statistik_nilai">Nilai</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap Kunjungan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/statistik">Hari ini</a></li>
                                <li><a href="/rekap_list_hari_kunj">Harian</a></li>
                                <li><a href="/rekap_list_bulan_kunj">Bulanan</a></li>
                                <li><a href="/rekap_list_tahun_kunj">Tahunan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap RM</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/rekap_hari_ini">Hari ini</a></li>
                                <li><a href="/rekap_list_hari">Harian</a></li>
                                <li><a href="/rekap_list_bulan">Bulanan</a></li>
                                <li><a href="/rekap_list_tahun">Tahunan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap Farmasi</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/rekap_hari_ini_apt">Hari ini</a></li>
                                <li><a href="/rekap_list_hari_apt">Harian</a></li>
                                <li><a href="/rekap_list_bulan_apt">Bulanan</a></li>
                                <li><a href="/rekap_list_tahun_apt">Tahunan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap Keuangan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/indexkeuangan">Hari ini</a></li>
                                <li><a href="/rekap_harian">Harian</a></li>
                                <li><a href="/rekap_bulanan">Bulanan</a></li>
                                <li><a href="/rekap_tahunan">Tahunan</a></li>
                            </ul>
                        </li>

                        @endif

                        @if (Session::get('akses') == 'rm')
                        <li> <a class="waves-effect waves-dark" href="/indexrekamedis"><i class="icon-cursor"></i><span class="hide-menu">Input Data</span></a>
                        <li> <a class="waves-effect waves-dark" href="/pasien_list"><i class="icon-user"></i><span class="hide-menu">Data Pasien</span></a>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/rekap_hari_ini">Hari ini</a></li>
                                <li><a href="/rekap_list_hari">Harian</a></li>
                                <li><a href="/rekap_list_bulan">Bulanan</a></li>
                                <li><a href="/rekap_list_tahun">Tahunan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-folder-alt"></i><span class="hide-menu">Master ICD</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/icd_all">ICD X</a></li>
                            </ul>
                        </li>
                        @endif

                        @if (Session::get('akses') == 'apotik')
                        <li> <a class="waves-effect waves-dark" href="/indexapotik"><i class="icon-cursor"></i><span class="hide-menu">Input Data</span></a>
                        <li> <a class="waves-effect waves-dark" href="/pasien_list"><i class="icon-user"></i><span class="hide-menu">Pasien</span></a>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/rekap_hari_ini_apt">Hari ini</a></li>
                                <li><a href="/rekap_list_hari_apt">Harian</a></li>
                                <li><a href="/rekap_list_bulan_apt">Bulanan</a></li>
                                <li><a href="/rekap_list_tahun_apt">Tahunan</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-folder-alt"></i><span class="hide-menu">Master Obat</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/obat_all">Data Obat</a></li>
                            </ul>
                        </li>
                        @endif

                        @if (Session::get('akses') == 'kasir')
                        <li> <a class="waves-effect waves-dark" href="/kasir"><i class="icon-user"></i><span class="hide-menu">Transaksi</span></a>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-book-open"></i><span class="hide-menu">Rekap</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="/indexkeuangan">Hari ini</a></li>
                                <li><a href="/rekap_harian">Harian</a></li>
                                <li><a href="/rekap_bulanan">Bulanan</a></li>
                                <li><a href="/rekap_tahunan">Tahunan</a></li>
                            </ul>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="/tarif"><i class="icon-drawar"></i><span class="hide-menu">Master Tarif</span></a>
                            @endif

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2019 - Sistem Informasi Puskesmas - Puskesmas Tebas
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="/assets/node_modules/popper/popper.min.js"></script>
    <script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="/dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="/dist/js/pages/morris-data.js"></script>
    <script src="/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Popup message jquery -->
    <script src="/assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <!-- jQuery peity -->
    <script src="/assets/node_modules/peity/jquery.peity.min.js"></script>
    <script src="/assets/node_modules/peity/jquery.peity.init.js"></script>
    <script src="/dist/js/dashboard1.js"></script>
    <script src="/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
    {{-- <script src="/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script src="/assets/node_modules/sweetalert/jquery.sweet-alert.custom.js"></script> --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="/assets/node_modules/moment/moment.js"></script>
    <script src="/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="/assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/assets/node_modules/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="/assets/node_modules/html5-editor/bootstrap-wysihtml5.js"></script>
    <script src="/assets/node_modules/Chart.js/chartjs.init.js"></script>
    <script src="/assets/node_modules/Chart.js/Chart.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#mdate').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false
            });
            $('#myTable').DataTable();


            $(document).ready(function() {



                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>
</body>

</html>