
@extends('root/root')

@section('content')

  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Edit Data Obat</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Edit Data Obat</li>
              </ol>

          </div>
      </div>
  </div>

  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Edit Data Obat</h4>

                  <div class="table-responsive m-t-40">
                              @foreach ($data as $data)
                              <form method="post" action="/simpus/update_obat">
                                @csrf
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Obat</label>
                                    <input type="text" class="form-control" name="nama_obat" placeholder="ID Dokter" value="{{ $data->nama_obat}}">
                                  </div>

                                  <input type="text" name="id" value="{{ $data->id }}" hidden>
                                  <button type="submit" class="btn btn-info waves-effect" >Simpan</button>
                              </form>
                              @endforeach

                          <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                  </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


@endsection
