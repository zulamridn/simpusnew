@extends('root/root')

@section('content')
  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Rekap Penyakit</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Data Obat Terbanyak Hari ini</li>
              </ol>
          </div>
      </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
            <div class="card-body">
                <h5 class="card-title m-b-0">Daftar List Penyakit</h5>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:30px">#</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no = 1; ?>
                          @foreach ($obat as $obat)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $obat->nama_obat}}</td>
                                <td>{{ $obat->total}}</td>
                            </tr>
                          @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>


@endsection
