@extends('root/root')

@section('content')

<div class="row page-titles">
  <div class="col-md-5 align-self-center">
    <h4 class="text-themecolor">Loket Pelayanan Kesehatan {{ Session::get('poli') }} / {{ Session::get('dokter') }}</h4>
  </div>
  <div class="col-md-7 align-self-center text-right">

  </div>
</div>

<div class="row">

  <div class="col-lg-6 col-md-6">

    <div class="row">
      <div class="col-lg-4 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading" id="total">{{ $data->total }}</h1>
          <hr>
          <p class="mb-0">Total Antrian</p>
        </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-4 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading" id="dipanggil">{{ $data->dipanggil }} </h1>
          <hr>
          <p class="mb-0">Dipanggil</p>
        </div>
      </div>
      <!-- Column -->
      <!-- Column -->

      <div class="col-lg-4 col-md-6">
        <div class="alert alert-info text-center" role="alert" style="height:100%">
          <h1 class="alert-heading" id="sisa">{{ $data->sisa }}</h1>
          <hr>
          <p class="mb-0">Sisa Antrian</p>
        </div>
      </div>
    </div>
  </div>



  <!-- Column -->
  <!-- Column -->
  <div class="col-lg-6 col-md-6">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-row">
          <div class="row" style="width:100%">
            <div class="col">
              <button style="width:100%;height:100px" class="btn btn-success" id="panggil">Panggil</button>
            </div>
            <!--<div class="col">-->
            <!--  <button style="width:100%;height:100px" class="btn btn-danger" id="ulang">Ulang</button>-->
            <!--</div>-->
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-6">
    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col">
            <div class="alert alert-success" role="alert">
              <h3 class="alert-heading">Data Antrian : <span id="nomorantrian" style="font-size:40px"> </span> / <span id="idantri" style="font-size:20px;color:red"> </span> / <span id="umum" style="font-size:30px"> </span> / <span id="onlinestatus" style="font-size:30px"> </span> </h3 <center>
              <input type="text" id="reg_par" hidden />
              <input type="text" id="nik_par" hidden />
              <input type="text" id="rm_par" hidden />
              <input type="text" id="namapoli" value="{{ Session::get('poli') }}" hidden />
              <input type="text" id="namadokter" value="{{ Session::get('dokter') }}" hidden />
              <input tyoe="text" id="kode_antrian" hidden />
              <input type="text" id="nomor_antrian" hidden />
              <input type="text" value="{{$data->kode_antrian_poli}}" id="kode_antrian_poli" hidden />
              </center>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="card">
      <div class="card-header">
        Data Pasien
      </div>
      <div class="card-body">

        <table style="font-size:18px">
        <tr>
            <td>NIK</td>
            <td style="padding-left:9px;padding-right:9px">:</td>
            <td id="nik"></td>
          </tr>
          <tr>
            <td>Nomor RM</td>
            <td style="padding-left:9px;padding-right:9px">:</td>
            <td id="rm"></td>
          </tr>
          <tr>
            <td>Nama Lengkap</td>
            <td style="padding-left:9px;padding-right:9px">:</td>
            <td id="nama"></td>
          </tr>
          <tr>
            <td>Umur</td>
            <td style="padding-left:9px;padding-right:9px">:</td>
            <td id="umur"></td>
          </tr>
          <tr>
            <td>Jenis Kelamin</td>
            <td style="padding-left:9px;padding-right:9px">:</td>
            <td id="jk"></td>
          </tr>
          <tr>
            <td>Kunjungan</td>
            <td style="padding-left:9px;padding-right:9px">:</td>
            <td id="kunjungan"></td>
          </tr>
        </table>

      </div>
    </div>


  </div>

  <div class="col-6">
    <div class="card">
      <div class="card-body">
        <div class="card" style="border:1px solid #dfe6e9">
          <div class="card-header">
            Keluhan Pasien
          </div>
          <div class="card-body">
            <form method="post">
              <div class="form-group">
                <textarea class="keluhan form-control textarea" rows="5" placeholder="Keluhan Pasien" name="keluhan" id="keluhan" required></textarea>
              </div>
            </form>
          </div>
        </div>

        <div class="card" style="border:1px solid #dfe6e9">
          <div class="card-header">
            Diagnosa Dokter
          </div>
          <div class="card-body">
            <form method="post">
              <div class="form-group">
                <textarea class="diagnosa form-control textarea" rows="5" placeholder="Diagnosa Dokter" name="diagnosa" id="diagnosa" required></textarea>
              </div>
            </form>
          </div>
        </div>

        <div class="card" style="border:1px solid #dfe6e9">
          <div class="card-header">
            Catatan Dokter
          </div>
          <div class="card-body">
            <form method="post">
              <div class="form-group">
                <textarea class="catatan form-control textarea" rows="5" placeholder="Catatan Dokter" name="catatan" id="catatan" required></textarea>
              </div>
            </form>
          </div>
        </div>

        <div class="card" style="border:1px solid #dfe6e9">
          <div class="card-header">
            Tindakan
          </div>
          <div class="card-body">
            <div class="border" style="width:100%; height:150px; border:1px solid grey; padding:20px">
              <select class="form-control demo" multiple>
                @foreach ($tarif as $tarif )
                <option value="{{ $tarif->id}}">{{ $tarif->nama_tindakan}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <button class="btn btn-info btn-block" id="simpandata">Simpan</button>

      </div>

    </div>
  </div>
</div>


{{-- <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script> --}}
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="/assets/tokenize2.js"></script>
<script type="text/javascript">
  $('.demo').tokenize2();


  $('#panggil').click(function() {

    $.ajax({
      type: "GET",
      url: "/api/panggilpoli",
      cache: false,
      success: function(data) {
        if (data.status === '0') {
          swal("Belum Ada Pasien", "Belum ada pasien mengantri hari ini", "error");
        } else {
          if (data.sisa === '0') {
            //$('.keluhan').val("Set A");
            swal("Antrian Habis", "Antrian hari ini habis", "error");
          } else {
            $('#dipanggil').text(data.dipanggil);
            $('#sisa').text(data.sisa);
            $('#total').text(data.dipanggil + data.sisa);
            $('#idantri').text(data.data[0].kode_antrian);
            $('#nik').text(data.data[0].nik);
            $('#rm').text(data.data[0].no_rm);
            $('#nama').text(data.data[0].nama);
            $('#umur').text(data.data[0].umur + " tahun");
            $('#kunjungan').text(data.data[0].kunjungan);
            $('#reg_par').val(data.data[0].kode_antrian);
            $('#nik_par').val(data.data[0].nik);
            $('#rm_par').val(data.data[0].no_rm);
            $('#kode_antrian').val(data.data[0].kode_antrian_poli);
            $('#nomor_antrian').val(data.data[0].antrian_poli);

            if (data.data[0].online == '0') {
              $('#onlinestatus').text("Offline");
            } else {
              $('#onlinestatus').text("Online");
            }

            if (data.data[0].jenis_kelamin === 'L') {
              $('#jk').text("Laki Laki");
            } else {
              $('#jk').text("Perempuan");
            }

            if (data.data[0].bpjs == '0') {
              $('#umum').text('Umum');
            } else {
              $('#umum').text('BPJS');
            }

            if (data.data[0].antrian_poli < 10) {
              $("#nomorantrian").text(data.data[0].kode_antrian_poli + "00" + data.data[0].antrian_poli);
            } else if (data.data[0].antrian_poli >= 10 && data.data[0].antrian_poli < 100) {
              $("#nomorantrian").text(data.data[0].kode_antrian_poli + "0" + data.data[0].antrian_poli);
            } else {
              $("#nomorantrian").text(data.data[0].kode_antrian_poli + data[0].antrian_poli);
            }

            $('#keluhan').val("");
            $('#diagnosa').val("");
            $('#catatan').val("");


            swal("Panggilan Berhasil", "Tunggu pasien, atau klik tombol ulang jika tidak terdengar", "success");
          }
        }
        console.log(data);

      }
    });
  })

  $('#ulang').click(function() {
    let kode_antrian = $('#kode_antrian').val();
    let no_antrian = $('#nomor_antrian').val();
    if (no_antrian == '' || no_antrian == '0') {
      swal("Belum Ada Pasien", "Belum ada pasien mengantri hari ini", "error");
    } else {
      $.ajax({
        type: "GET",
        url: "/api/ulangpanggilpoli/" + kode_antrian + "/" + no_antrian,
        cache: false,
        success: function(data) {
          if (data.status == '1') {
            swal("Panggilan Berhasil", "Tunggu pasien, atau klik tombol ulang jika tidak terdengar", "success");
          } else {
            swal("Belum Ada Pasien", "Belum ada pasien mengantri hari ini", "error");
          }
        }
      })
    }

  })

  $('#simpandata').click(function() {

    
          

    let reg = $('#idantri').text();
    let nik = $('#nik').text();
    let rm = $('#rm').text();
    let nama_poli = $('#namapoli').val();
    let nama_dokter = $('#namadokter').val();
    let keluhan = $('.keluhan').val();
    let diagnosa = $('.diagnosa').val();
    let catatan = $('.catatan').val();
    let tindakan = $('.demo').val();

    
    if(keluhan == '' || diagnosa == '' || catatan == '' || tindakan == ''){
      alert("Lengkapi Data")
    }else{
      $.ajax({
      type: "GET",
      url: "/api/simpandatapoli/" + reg + "/" + rm + "/" + nik + "/" + nama_poli + "/" + nama_dokter + "/" + keluhan + "/" + diagnosa + "/" + catatan + "/" + tindakan,
      //data : { registrasi : reg, nik : nik, rm : rm, nama_poli : nama_poli, nama_dokter : nama_dokter, keluhan : keluhan, diagnosa : diagnosa, catatan : catatan },
      cache: false,
      success: function(data) {

        if (data.status === '1') {
          swal("Simpan Berhasil", "Diagnosa Pasien Berhasil di Simpan", "success");
        }else{
          swal("Kesalahan", "No Registrasi ini sudah tersimpan", "error");
        }

      }
    });
    }
    //alert(tindakan);
  })

  $(document).ready(function() {
    // Mengambil data antrian terakhir
    let kode_antrian = $('#kode_antrian_poli').val();
    console.log(kode_antrian);
    $.ajax({
      type: "GET",
      url: "/api/onloadpoli/" + kode_antrian,
      cache: false,
      success: function(data) {
        console.log(data);
        if (data.status === '0') {
          $("#nomorantrian").text('-');
          $('#idantri').text("-");
          $('#onlinestatus').text("-");
          $('#umum').text('-');
        } else {
          $('#idantri').text(data.antrian.kode_antrian);
          $('#rm').text(data.pasien.no_rm);
          $('#nik').text(data.pasien.nik);
          $('#nama').text(data.pasien.nama);
          $('#umur').text(data.antrian.umur + " tahun");

          if (data.online == '0') {
            $('#onlinestatus').text("Offline");
          } else {
            $('#onlinestatus').text("Online");
          }

          if (data.pasien.jenis_kelamin === 'L') {
            $('#jk').text("Laki Laki");
          } else {
            $('#jk').text("Perempuan");
          }

          if (data.bpjs == '0') {
            $('#umum').text('Umum');
          } else {
            $('#umum').text('BPJS');
          }

          if (data.dipanggil.dipanggil < 10) {
            $("#nomorantrian").text(kode_antrian + "00" + data.dipanggil.dipanggil);
          } else if (data.nomor_antrian >= 10 && data.nomor_antrian < 100) {
            $("#nomorantrian").text(kode_antrian + "0" + data.dipanggil.dipanggil);
          } else {
            $("#nomorantrian").text(kode_antrian + "0" + data.dipanggil.dipanggil);
          }
        }

      }
    });

    // $(".labelpoli").click(function(){
    //     var radioValue = $(this).attr('name');
    //     $('#kode_antrian_par').val(radioValue);
    //     console.log(radioValue);
    //   });
    //
    // $("#simpancetak").click(function(){
    //
    //   var kode_antrian = $('#kode_antrian_par').val();
    //   var nik = $('#nik_par').val();
    //   var id_antrian = $('#id_par').val();
    //
    //   if(kode_antrian === '' || nik === '' || id_antrian === ''){
    //     swal("Kesalahan", "Lengkapi DAta Anda", "error");
    //   }else{
    //
    //     $.ajax({
    //       type: "GET",
    //       url: "/api/antrianpoli/"+id_antrian+"/"+nik+"/"+kode_antrian,
    //       cache: false,
    //       success: function(data){
    //         if(data.status === '1'){
    //           swal("Simpan Data Berhasil", "Data pasien tersimpan di database", "success");
    //         }else{
    //           swal("Gagal Menyimpan", "Sudah terdapat pasien pada antrian ini", "error");
    //         }
    //         console.log(data.status);
    //
    //       }
    //     })
    //
    //   }
    //
    // })

  });
</script>

@endsection