@extends('root/root')

@section('content')



  <div class="row page-titles">
      <div class="col-md-5 align-self-center">
          <h4 class="text-themecolor">Statistik Keuangan</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
          <div class="d-flex justify-content-end align-items-center">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                  <li class="breadcrumb-item active">Statistik Keuangan</li>
              </ol>

          </div>
      </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Statistik Nilai Sepanjang Masa</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphSepanjangMasa" style="width:100%; height:400px" ><canvas>
            </div>
          </div>

          <div class="card">
            <div class="card-body">
              <div class="table-responsive m-t-40">
                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th style="width:30px">#</th>
                              <th>Poli</th>
                              <th>Pasien</th>
                              <th>Nilai</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach ($nilaisepanjangmasa as $nilaisepanjangmasa)
                          <tr>
                              <td>{{ $no++ }}</td>
                              <td>{{ $nilaisepanjangmasa->nama_pelayanan }}  </td>
                              <td>{{ $nilaisepanjangmasa->total }}  </td>
                              <td>{{ $nilaisepanjangmasa->nilai  }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Statistik Nilai Hari ini</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphHariIni" style="width:100%; height:400px" ><canvas>
            </div>
          </div>

          <div class="card">
            <div class="card-body">
              <div class="table-responsive m-t-40">
                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th style="width:30px">#</th>
                              <th>Poli</th>
                              <th>Pasien</th>
                              <th>Nilai</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach ($nilai as $nilai)
                          <tr>
                              <td>{{ $no++ }}</td>
                              <td>{{ $nilai->nama_pelayanan }}  </td>
                              <td>{{ $nilai->total }}  </td>
                              <td>{{ $nilai->nilai  }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Data Penilaian Bulan ini</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphBulanIni" style="width:100%; height:400px" ><canvas>
            </div>
          </div>

          <div class="card">
            <div class="card-body">
              <div class="table-responsive m-t-40">
                  <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                            <th style="width:30px">#</th>
                            <th>Poli</th>
                            <th>Pasien</th>
                            <th>Nilai</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        @foreach ($nilaibulan as $nilaibulan)
                          <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $nilaibulan->nama_pelayanan }}  </td>
                            <td>{{ $nilaibulan->total }}  </td>
                            <td>{{ $nilaibulan->nilai  }}</td>
                          </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="card">
        <div class="card-body">
          {{-- Statistik Per Tanggal --}}
          <h4>Data Penilaian Tahun Ini</h4>
          <div class="card">
            <div class="card-body">
              <canvas id="graphTahunIni" style="width:100%; height:400px" ><canvas>
            </div>
          </div>
      <div class="card">
        <div class="card-body">
          <div class="table-responsive m-t-40">
              <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                        <tr>
                            <th style="width:30px">#</th>
                            <th>Poli</th>
                            <th>Pasien</th>
                            <th>Nilai</th>
                        </tr>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    @foreach ($nilaitahun as $nilaitahun)
                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $nilaitahun->nama_pelayanan }}  </td>
                        <td>{{ $nilaitahun->total }}  </td>
                        <td>{{ $nilaitahun->nilai  }}</td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>



    </div>
  </div>
    <script src="/simpus/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
  <script src="/simpus/assets/node_modules/Chart.js/chartjs.init.js"></script>
  <script src="/simpus/assets/node_modules/Chart.js/Chart.min.js"></script>
  <script>
    var ctx1 = document.getElementById("graphSepanjangMasa").getContext("2d");
    var ctx2 = document.getElementById("graphHariIni").getContext("2d");
    var ctx3 = document.getElementById("graphBulanIni").getContext("2d");
    var ctx4 = document.getElementById("graphTahunIni").getContext("2d");

    $('document').ready(function(){

      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_nilai_alltime",
        cache: false,
        success: function(data){
          //console.log(data)
          let nama_pelayanan = [];
          let nilai = [];
          for(var i = 0; i < data.length; i++) {
            nama_pelayanan.push(data[i].nama_pelayanan)
            nilai.push(data[i].nilai)
            console.log(nilai)
          }
          var data1 = {
              labels: nama_pelayanan,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: nilai
                  }

              ]
          };

          var chart1 = new Chart(ctx1).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_nilai_hari_ini",
        cache: false,
        success: function(data){
          //console.log(data)
          let nama_pelayanan = [];
          let nilai = [];
          for(var i = 0; i < data.length; i++) {
            nama_pelayanan.push(data[i].nama_pelayanan)
            nilai.push(data[i].nilai)
            //console.log('tanggal:'+tanggal);
          }
          var data1 = {
              labels: nama_pelayanan,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: nilai
                  }

              ]
          };

          var chart2 = new Chart(ctx2).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_nilai_bulan_ini",
        cache: false,
        success: function(data){
          //console.log(data)
          let nama_pelayanan = [];
          let nilai = [];
          for(var i = 0; i < data.length; i++) {
            nama_pelayanan.push(data[i].nama_pelayanan)
            nilai.push(data[i].nilai)
            //console.log('tanggal:'+tanggal);
          }
          var data1 = {
              labels: nama_pelayanan,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: nilai
                  }

              ]
          };

          var chart3 = new Chart(ctx3).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


      $.ajax({
        type: "GET",
        url: "/simpus/api/statistik_nilai_tahun_ini",
        cache: false,
        success: function(data){
          //console.log(data)
          let nama_pelayanan = [];
          let nilai = [];
          for(var i = 0; i < data.length; i++) {
            nama_pelayanan.push(data[i].nama_pelayanan)
            nilai.push(data[i].nilai)
            //console.log('tanggal:'+tanggal);
          }
          var data1 = {
              labels: nama_pelayanan,
              datasets: [
                  {
                      label: "My First dataset",
                      fillColor: "rgba(152,235,239,0.8)",
                      strokeColor: "rgba(152,235,239,0.8)",
                      pointColor: "rgba(152,235,239,1)",
                      pointStrokeColor: "#fff",
                      pointHighlightFill: "#fff",
                      pointHighlightStroke: "rgba(152,235,239,1)",
                      data: nilai
                  }

              ]
          };

          var chart4 = new Chart(ctx4).Line(data1, {
              scaleShowGridLines : true,
              scaleGridLineColor : "rgba(0,0,0,.005)",
              scaleGridLineWidth : 0,
              scaleShowHorizontalLines: true,
              scaleShowVerticalLines: true,
              bezierCurve : true,
              bezierCurveTension : 0.4,
              pointDot : true,
              pointDotRadius : 4,
              pointDotStrokeWidth : 1,
              pointHitDetectionRadius : 2,
              datasetStroke : true,
          tooltipCornerRadius: 2,
              datasetStrokeWidth : 2,
              datasetFill : true,
              legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              responsive: true
          });
        }
      })


    });
  </script>

@endsection
