@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Rekap Keuangan</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Keuangan</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Keuangan</h4>

                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>{{ $filter }}</th>
                                                <th>Total</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>No </th>
                                              <th>{{ $filter }}</th>
                                              <th>Total</th>
                                              <th>Detail</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($rekap as $rekap)
                                                <tr>
                                                  <td style="width:30px" > {{ $no++ }}   </td>
                                                  <td> @if($filter === "Tanggal") {{ date('d F Y', strtotime($rekap->filter)) }}
                                                  @elseif($filter === "Bulan")  {{ date('F', strtotime($rekap->bulan)) }} {{ $rekap->tahun}}
                                                      @else {{ $rekap->tahun }}
                                                      @endif </td>
                                                  <td> {{ number_format($rekap->total,0,',','.') }} </td>
                                                  <td><center>  @if($filter === "Tanggal") <a href="/simpus/rekap_harian_keu/{{ $rekap->filter }}" class="btn btn-info"><i class="fa fa-eye" ></i>&nbsp&nbsp&nbspDetail</a>
                                                                @elseif($filter === "Bulan") <a href="/simpus/rekap_bulanan_keu/{{$rekap->bulan}}/{{$rekap->tahun}}" class="btn btn-info"><i class="fa fa-eye" ></i>&nbsp&nbsp&nbspDetail</a>
                                                                @else <a href="/simpus/rekap_tahunan_keu/{{$rekap->tahun}}" class="btn btn-info"><i class="fa fa-eye" ></i>&nbsp&nbsp&nbspDetail</a>
                                                                @endif </td> </center></td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection
