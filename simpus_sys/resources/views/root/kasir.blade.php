@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Kasir</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Kasir</li>
                            </ol>
                             <a class="btn btn-info d-none d-lg-block m-l-15" style="color:white" data-toggle="modal" data-target="#printersetting"><i class="fa fa-print"></i> Printer Setting</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Kasir</h4>

                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>No Registrasi</th>
                                                <th>Nama Pasien</th>
                                                <th>Umur</th>
                                                <th>Jenis Kelamin</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                              <th>No </th>
                                              <th>No Registrasi</th>
                                              <th>Nama Pasien</th>
                                              <th>Umur</th>
                                              <th>Jenis Kelamin</th>
                                              <th>#</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($data as $data)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $data->kode_antrian }} </td>
                                                  <td> {{ $data->nama }} </td>
                                                  <td> {{ $data->umur }} </td>
                                                  <td> {{ $data->jenis_kelamin }} </td>
                                                  <td><center> <a href='/simpus/kasirdetail/{{ $data->kode_antrian }}' class="btn btn-info" style="color:white"><i class="fa fa-eye"></i>&nbsp&nbsp Detail</a></center></td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>

                                </div>
                                <div class="table-responsive m-t-40">
                                        <div id="printersetting" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="printersetting" aria-hidden="true">
                                          <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Printer Setting</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                              </div>
                                              <div class="modal-body">
                                                @foreach ($printer as $printer)
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">App Key</label>
                                                  <input type="text" class="form-control" name="app_key" id="app_key" width="100px" required value="{{ $printer->app_key }}">
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail1">Application Port</label>
                                                  <input type="text" class="form-control" name="app_port" id="app_port" width="100px" required value="{{ $printer->app_port }}">
                                                </div>
                                                @endforeach
                                              </div>
                                              <div class="modal-footer">
                                                <button class="btn btn-info waves-effect" id="printsetting">Simpan Setting</button>
                                              </div>
                                
                                            </div>
                                            <!-- /.modal-content -->
                                          </div>
                                          <!-- /.modal-dialog -->
                                        </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/recta/dist/recta.js"></script>
                <script>
                $('#printsetting').click(function() {

                    let app_key = $('#app_key').val();
                    let app_port = $('#app_port').val();
                
                    $.ajax({
                      type: "GET",
                      url: "/simpus/api/setprint/Kasir/" + app_key + "/" + app_port,
                      cache: false,
                      success: function(data) {
                        if (data.status == '1') {
                          swal("Setting Berhasil", "Printer Berhasil di Setting", "success");
                        } else {
                          swal("Setting gagal", "Silahkan Coba Lagi", "error");
                        }
                      }
                    })
                })
                </script>

@endsection
