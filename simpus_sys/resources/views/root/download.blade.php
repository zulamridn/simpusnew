@extends('root/root')

@section('content')

                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Download</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Download</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">


                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No </th>
                                                <th>Nama File</th>
                                                <th>Link Download</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                          <tr>
                                                <th>No </th>
                                                <th>Nama File</th>
                                                <th>Link Download</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>

                                              <?php $no = 1; ?>
                                              @foreach ($data as $data)
                                                <tr>
                                                  <td> {{ $no++ }}   </td>
                                                  <td> {{ $data->nama_file }} </td>
                                                  <td> {{ $data->alamat }} <a href={{$data->link_file}} target="_blank" > Download </a></td>
                                                  <td> {{ $data->keterangan }} </td>
                                                </tr>
                                              @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-lg-6">

                  </div>
                  <div class="col-lg-6">

                  </div>
                </div>
@endsection
