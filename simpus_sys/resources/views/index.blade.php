<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.png">
    <title>Halaman Login Pendaftaran</title>


    <link href="/dist/css/pages/login-register-lock.css" rel="stylesheet">

    <link href="/dist/css/style.min.css" rel="stylesheet">
</head>

<body class="skin-blue card-no-border">
  @foreach ($data as $data)


    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">{{ $data->nama }}</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{$data->back2}});">
            <div class="login-box card">
                <div class="card-body">
                    <center><img src="{{ $data->logo }}" class="img-responsive" style="width:20%; margin-bottom:10px"/></center>
                    <form class="form-horizontal form-material" id="loginform">
                        @csrf
                        <center><h5 class="box-title m-b-20">{{ $data->nama }}</h5></center>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Username" id="username"> </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Password" id="password"> </div>
                        </div>


                    </form>
                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn btn-block btn-lg btn-info" id="login">Log In</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @endforeach
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/assets/node_modules/popper/popper.min.js"></script>
    <script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });

    $('#login').click(function(){
      let username = $('#username').val();
      let password = $('#password').val();

      $.ajax({
        type: "GET",
        url: "/api/loginall/"+username+"/"+password,
        //data : { username : username, password : password },
        success: function(data){
          if(data.status === '1'){
            if(data.akses === 'pendaftaran'){
              window.location.href = '/pendaftaran';
            }else if (data.akses === 'rm') {
              window.location.href = '/indexrekamedis';
            }else if (data.akses === 'apotik') {
              window.location.href = '/indexapotik';
            }else if (data.akses === 'kasir') {
              window.location.href = '/kasir';
            }else if (data.akses === 'statistik') {
              window.location.href = '/statistik';
            }else{
              window.location.href = '/statistik';
            }

          }else{
            alert("Password / Username Salah");
          }
          //console.log(data);
        }
      })

    })
    </script>

</body>

</html>
