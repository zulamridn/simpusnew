<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/vendor/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


$nomor = $_GET['nomor'];
$rm = $_GET['rm'];

try {
    // Enter the share name for your printer here, as a smb:// url format
   $connector = new WindowsPrintConnector("EPSONT82");
      $printer = new Printer($connector);
      $printer -> setJustification(Printer::JUSTIFY_CENTER);
      $printer -> text("Puskesmas Sambas\n");
      $printer -> text("Dinas Kesehatan Kabupaten Sambas\n");
      $printer -> text("\n");
      $printer -> setTextSize(6,6);
      $printer -> text($nomor."\n");
      $printer -> text("\n");
      $printer -> cut();
      $printer -> setJustification(Printer::JUSTIFY_LEFT);
      $printer -> setTextSize(2,2);
      $printer -> text("No :".$nomor."\n");
      //$printer -> text("Nama :".$data['nama']."\n");
      $printer -> text("RM :".$rm."\n");
      $printer -> text("\n");
      $printer -> cut();
      $printer -> close();
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}
